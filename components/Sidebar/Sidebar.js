/*eslint-disable*/
import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { useRouter } from 'next/router'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Icon from '@material-ui/core/Icon'
// core components
import AdminNavbarLinks from 'components/Navbars/AdminNavbarLinks.js'
import RTLNavbarLinks from 'components/Navbars/RTLNavbarLinks.js'
import Button from '@material-ui/core/Button'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import Cookies from 'js-cookie'
import Router, { withRouter } from 'next/router'
import Image from 'next/image'

//custom google material icon
import ManageAccounts from '../../assets/svg/manage_accounts_white_24dp.svg'
import AdminPanel from '../../assets/svg/admin_panel_settings_white_24dp.svg'

import styles from 'assets/jss/nextjs-material-dashboard/components/sidebarStyle.js'

export default function Sidebar (props) {
  // used for checking current route
  const router = useRouter()
  // creates styles for this component
  const useStyles = makeStyles(styles)
  const classes = useStyles()
  // verifies if routeName is the one active (in browser input)
  function activeRoute (routeName) {
    console.log('router.route---', router.route)
    if (
      (router.route === '/admin/add-new-dealer' ||
        router.route === '/admin/edit-dealer') &&
      routeName === '/admin/dealers'
    ) {
      return true
    } else if (
      (router.route === '/admin/add-new-allotment' ||
        router.route === '/admin/details-allotment') &&
      routeName === '/admin/allotments'
    ) {
      return true
    } else if (
      router.route === '/admin/warehouse-details-allotment' &&
      routeName === '/admin/warehouse-allotments'
    ) {
      return true
    } else {
      return router.route.indexOf(routeName) > -1 ? true : false
    }
  }
  const handleLogout = () => {
    Cookies.remove('cToken')
    Router.push('/admin/login')
  }
  const { color, logo, image, logoText, routes } = props
  var links = (
    <List className={classes.list}>
      {routes.map((prop, key) => {
        console.log('prop.path---', prop.layout + prop.path)
        var activePro = ' '
        var listItemClasses
        if (prop.path === '/upgrade-to-pro') {
          activePro = classes.activePro + ' '
          listItemClasses = classNames({
            [' ' + classes[color]]: true
          })
        } else {
          listItemClasses = classNames({
            // [" " + classes[color]]: activeRoute(prop.layout + prop.path),
            [' ' + classes[color]]: activeRoute(prop.layout + prop.path)
          })
        }
        const whiteFontClasses = classNames({
          [' ' + classes.whiteFont]:
            activeRoute(prop.layout + prop.path) ||
            prop.path === '/upgrade-to-pro'
        })
        return (
          <>
            {prop?.permissions &&
            prop?.permissions.includes(Cookies.get('cRole')) ? (
              <Link href={prop.layout + prop.path} key={key}>
                <a className={activePro + classes.item}>
                  <ListItem
                    button
                    className={classes.itemLink + listItemClasses}
                    style={{ display: 'flex' }}
                  >
                    {typeof prop.icon === 'string' ? (
                      // <Icon
                      //   className={classNames(
                      //     classes.itemIcon,
                      //     whiteFontClasses,
                      //     {
                      //       [classes.itemIconRTL]: props.rtlActive
                      //     }
                      //   )}
                      // >
                      //   {prop.icon}
                      // </Icon>
                      prop.icon === 'ManageAccounts' ? (
                        <img
                          className={classNames(
                            classes.itemIcon,
                            whiteFontClasses,
                            {
                              [classes.itemIconRTL]: props.rtlActive
                            }
                          )}
                          style={{ marginRight: '15px' }}
                          src={ManageAccounts}
                          height={26}
                          width={30}
                        />
                      ) : prop.icon === 'AdminPanel' ? (
                        <img
                          className={classNames(
                            classes.itemIcon,
                            whiteFontClasses,
                            {
                              [classes.itemIconRTL]: props.rtlActive
                            }
                          )}
                          style={{ marginRight: '15px' }}
                          src={AdminPanel}
                          height={30}
                          width={30}
                        />
                      ) : (
                        ''
                      )
                    ) : (
                      <prop.icon
                        className={classNames(
                          classes.itemIcon,
                          whiteFontClasses,
                          {
                            [classes.itemIconRTL]: props.rtlActive
                          }
                        )}
                      />
                      // <Image src={sidebarLogo} height={30} width={30} />
                    )}
                    {console.log('routes.permissions---', prop?.permissions)}
                    <ListItemText
                      style={{ fontWeight: 'bold' }}
                      primary={props.rtlActive ? prop.rtlName : prop.name}
                      className={classNames(
                        classes.itemText,
                        whiteFontClasses,
                        {
                          [classes.itemTextRTL]: props.rtlActive
                        }
                      )}
                      disableTypography={true}
                    />
                  </ListItem>
                </a>
              </Link>
            ) : (
              ''
            )}
          </>
        )
      })}
      <ListItem style={{ position: 'absolute', bottom: '20px' }}>
        <Button
          variant='contained'
          color='secondary'
          className={classes.button}
          startIcon={<ExitToAppIcon />}
          onClick={handleLogout}
          style={{ background: '#F0383B' }}
        >
          Log out
        </Button>
      </ListItem>
    </List>
  )
  var brand = (
    <div className={classes.logo}>
      <a
        href='http://www.tcb.gov.bd/'
        className={classNames(classes.logoLink, {
          [classes.logoLinkRTL]: props.rtlActive
        })}
        target='_blank'
      >
        <div className={classes.logoImage}>
          <img src={logo} alt='logo' className={classes.img} />
        </div>
        <span style={{ color: '#fff' }}>{logoText}</span>
      </a>
    </div>
  )
  return (
    <div>
      <Hidden mdUp implementation='css'>
        <Drawer
          variant='temporary'
          anchor={props.rtlActive ? 'left' : 'right'}
          open={props.open}
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: props.rtlActive
            })
          }}
          onClose={props.handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>
            {props.rtlActive ? <RTLNavbarLinks /> : <AdminNavbarLinks />}
            {links}
          </div>
          {image !== undefined ? (
            <div
              className={classes.background}
              // style={{ backgroundImage: 'url(' + image + ')' }}
            />
          ) : null}
        </Drawer>
      </Hidden>
      <Hidden smDown implementation='css'>
        <Drawer
          anchor={props.rtlActive ? 'right' : 'left'}
          variant='permanent'
          open
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: props.rtlActive
            })
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              // style={{ backgroundImage: "url(" + image + ")" }}
              // style={{ background: "#333333" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
    </div>
  )
}

Sidebar.propTypes = {
  rtlActive: PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  bgColor: PropTypes.oneOf([
    'white',
    'purple',
    'blue',
    'green',
    'orange',
    'red'
  ]),
  logo: PropTypes.string,
  image: PropTypes.string,
  logoText: PropTypes.string,
  routes: PropTypes.arrayOf(PropTypes.object),
  open: PropTypes.bool
}
