import { useState } from 'react'

const AllotmentValidation = async (values, successCallback, errorCallback) => {
  // do stuff
  // if successful
  if (true) successCallback()
  else errorCallback()
}

const initialFormValues = {
  dealerName: '',
  shopName: '',
  shopAddress: '',
  mobileNo: '',
  district: '',
  upazila: '',
  distanceZonal: '',
  allotmentDate: '',
  // contractRenewalDate: '',
  contractExpiryDate: '',
  contractStatus: '',
  relationType: '',
  familyDealerId: '',

  email: '',
  message: '',
  formSubmitted: false,
  success: false
}

export const useFormControls = () => {
  const [values, setValues] = useState(initialFormValues)
  const [errors, setErrors] = useState({})

  const validate = (fieldValues = values) => {
    let temp = { ...errors }

    if ('dealerName' in fieldValues)
      temp.dealerName = fieldValues.dealerName ? '' : 'This field is required.'

    if ('shopName' in fieldValues)
      temp.shopName = fieldValues.shopName ? '' : 'This field is required.'

    if ('shopAddress' in fieldValues)
      temp.shopAddress = fieldValues.shopAddress
        ? ''
        : 'This field is required.'

    if ('mobileNo' in fieldValues) {
      temp.mobileNo = fieldValues.mobileNo ? '' : 'This field is required.'
      if (fieldValues.mobileNo.length == 1) {
        temp.mobileNo =
          fieldValues.mobileNo.charAt(0) == 0
            ? ''
            : 'Mobile number is not valid.'
      } else if (
        fieldValues.mobileNo.length > 1 &&
        fieldValues.mobileNo.length < 12
      ) {
        temp.mobileNo =
          fieldValues.mobileNo.charAt(1) == 1 &&
          fieldValues.mobileNo.charAt(0) == 0
            ? ''
            : 'Mobile number is not valid.'
      } else if (fieldValues.mobileNo.length > 11) {
        temp.mobileNo =
          fieldValues.mobileNo.length < 12
            ? ''
            : 'Mobile number must be 11 digit long.'
      }
    }

    if ('district' in fieldValues)
      temp.district = fieldValues.district ? '' : 'This field is required.'

    if ('upazila' in fieldValues)
      temp.upazila = fieldValues.upazila ? '' : 'This field is required.'

    if ('distanceZonal' in fieldValues)
      temp.distanceZonal = fieldValues.distanceZonal
        ? ''
        : 'This field is required.'

    if ('allotmentDate' in fieldValues)
      temp.allotmentDate = fieldValues.allotmentDate
        ? ''
        : 'This field is required.'

    // if ('contractRenewalDate' in fieldValues)
    //   temp.contractRenewalDate = fieldValues.contractRenewalDate ? '' : 'This field is required.'

    if ('contractExpiryDate' in fieldValues)
      temp.contractExpiryDate = fieldValues.contractExpiryDate
        ? ''
        : 'This field is required.'

    if ('contractStatus' in fieldValues)
      temp.contractStatus = fieldValues.contractStatus
        ? ''
        : 'This field is required.'

    if ('relationType' in fieldValues)
      temp.relationType = fieldValues.relationType
        ? ''
        : 'This field is required.'

    if ('familyDealerId' in fieldValues) {
      if (fieldValues.familyDealerId.length > 0) {
        temp.relationType = values.relationType ? '' : 'This field is required.'
        // if ('relationType' in fieldValues)
        //   temp.relationType = fieldValues.relationType ? '' : 'This field is required.'
      }
      if (fieldValues.familyDealerId.length == 0) {
        temp.relationType = ''
        // setValues({
        //   ...values,
        //   ['familyDealerId']: '',
        //   ['relationType']: ''
        // })
      }
    }

    if ('email' in fieldValues) {
      temp.email = fieldValues.email ? '' : 'This field is required.'
      if (fieldValues.email)
        temp.email = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(fieldValues.email)
          ? ''
          : 'Email is not valid.'
    }

    if ('message' in fieldValues)
      temp.message =
        fieldValues.message.length !== 0 ? '' : 'This field is required.'

    setErrors({
      ...temp
    })
  }

  const handleInputValue = e => {
    const { name, value } = e.target
    setValues({
      ...values,
      [name]: value
    })
    validate({ [name]: value })
  }

  // const handlePresetValue = (name, value) => {
  //   // const { name, value } = e.target
  //   setValues({
  //     ...values,
  //     [name]: value
  //   })
  //   // validate({ [name]: value })
  // }

  const handlePresetValue = data => {
    // const { name, value } = e.target
    setValues({
      ...values,
      dealerName: data.name,
      shopName: data.shop_name,
      shopAddress: data.shop_address,
      mobileNo: data.mobile_no,
      district: data.district,
      upazila: data.upazila,
      distanceZonal: data.distance_from_zonal,
      contractDate: data.contract_date,
      // contractRenewalDate: '',
      contractExpiryDate: data.contract_expiry_date,
      contractStatus: data.contract_status,
      relationType: data.family_relation_type,
      familyDealerId: data.family_dealer_id
    })
    // validate({ [name]: value })
  }

  const handleSuccess = () => {
    setValues({
      ...initialFormValues,
      formSubmitted: true,
      success: true
    })
  }

  const handleError = () => {
    setValues({
      ...initialFormValues,
      formSubmitted: true,
      success: false
    })
  }

  const formIsValid = (fieldValues = values) => {
    const isValid =
      //   fieldValues.dealerName &&
    //   fieldValues.shopName &&
    //   fieldValues.shopAddress &&
    //   fieldValues.mobileNo &&
      fieldValues.district &&
      fieldValues.upazila &&
      fieldValues.allotmentDate &&
      // fieldValues.distanceZonal &&
      // fieldValues.contractRenewalDate &&
    //   fieldValues.contractExpiryDate &&
      // {...(fieldValues.familyDealerId ? `${fieldValues.relationType} &&` : null)}
    //   fieldValues.contractStatus &&
      // fieldValues.email &&
      // fieldValues.message &&
      Object.values(errors).every(x => x === '')

    return isValid
  }

  const handleFormSubmit = async e => {
    e.preventDefault()
    const isValid = Object.values(errors).every(x => x === '') && formIsValid()
    if (isValid) {
      await PostContactForm(values, handleSuccess, handleError)
    }
  }

  return {
    values,
    errors,
    handleInputValue,
    handleFormSubmit,
    formIsValid,
    handlePresetValue
  }
}
