import { useState } from "react";

const PostContactForm = async (
  values,
  successCallback,
  errorCallback
) => {
  // do stuff
  // if successful
  if (true) successCallback();
  else errorCallback();
};

const initialFormValues = {
  fullName: "",
  zone: "",
  email: "",
  password: "",
  editPassword: "",
  role: "",
  message: "",
  formSubmitted: false,
  success: false
};

export const useAddUserFormControls = () => {
  const [values, setValues] = useState(initialFormValues);
  const [errors, setErrors] = useState({});

  const validate = (fieldValues = values) => {
    let temp = { ...errors };

    if ("fullName" in fieldValues)
      temp.fullName = fieldValues.fullName ? "" : "This field is required.";

    if ("zone" in fieldValues)
      temp.zone = fieldValues.zone ? "" : "This field is required.";

    if ("role" in fieldValues)
      temp.role = fieldValues.role ? "" : "This field is required.";

    if ("password" in fieldValues)
      temp.password = fieldValues.password ? "" : "Password field is required.";
      if (fieldValues.password)
        temp.password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(fieldValues.password)
          ? ""
          : "please use 8 or more characters with a mix of uppercase & lowercase letters, numbers & symboles";

    if ("editPassword" in fieldValues)
      temp.editPassword = fieldValues.editPassword ? "" : "Password field is required.";
      if (fieldValues.editPassword)
        temp.editPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(fieldValues.editPassword)
          ? ""
          : "please use 8 or more characters with a mix of uppercase & lowercase letters, numbers & symboles";

    if ("email" in fieldValues) {
      temp.email = fieldValues.email ? "" : "This field is required.";
      if (fieldValues.email)
        temp.email = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(fieldValues.email)
          ? ""
          : "Email is not valid.";
    }

    if ("message" in fieldValues)
      temp.message =
        fieldValues.message.length !== 0 ? "" : "This field is required.";

    setErrors({
      ...temp
    });
  };

  const handleInputValue = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    });
    validate({ [name]: value });
  };

  const handleSuccess = () => {
    setValues({
      ...initialFormValues,
      formSubmitted: true,
      success: true
    });
  };

  const handleError = () => {
    setValues({
      ...initialFormValues,
      formSubmitted: true,
      success: false
    });
  };

  const formIsValid = (fieldValues = values) => {
    const isValid =
      fieldValues.fullName &&
      fieldValues.password &&
      fieldValues.editPassword &&
      fieldValues.email &&
      fieldValues.message &&
      Object.values(errors).every((x) => x === "");

    return isValid;
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const isValid =
      Object.values(errors).every((x) => x === "") && formIsValid();
    if (isValid) {
      await PostContactForm(values, handleSuccess, handleError);
    }
  };

  return {
    values,
    errors,
    handleInputValue,
    handleFormSubmit,
    formIsValid
  };
};
