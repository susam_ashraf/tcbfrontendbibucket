import React, { useState } from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import CustomInput from 'components/CustomInput/CustomInput.js'
import Button from 'components/CustomButtons/Button.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardAvatar from 'components/Card/CardAvatar.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'

import avatar from 'assets/img/faces/marc.jpg'
import tcb_logo from 'assets/img/tcb_logo.png'

import TextField from '@material-ui/core/TextField'
import axios from 'axios'
import Router, { withRouter } from 'next/router'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import FormControl from '@material-ui/core/FormControl'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import FilledInput from '@material-ui/core/FilledInput'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormHelperText from '@material-ui/core/FormHelperText'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

import { useLoginFormControls } from '../../components/LoginFormControls/LoginFormControls'

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0'
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none'
  },
  error_txt: {
    color: '#f44336',
    margin: '0',
    fontSize: '0.75rem',
    marginTop: '3px',
    textAlign: 'left',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    fontWeight: '400',
    lineHeight: '1.66',
    marginLeft: '14px',
    marginRight: '14px',
    maxWidth: 'fit-content'
  }
}

function UserProfile () {
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()
  const [loginResponse, setLoginResponse] = useState()

  const [values, setValues] = React.useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false
  })

  const {
    handleInputValue,
    handleFormSubmit,
    formIsValid,
    errors
    // values
  } = useLoginFormControls()

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value })
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword })
  }

  const handleMouseDownPassword = event => {
    event.preventDefault()
  }

  const submitLogin = e => {
    e.preventDefault()

    if(!errors['email'] && !errors['password']) {
      const submitData = {
        email: email,
        password: password
      }
  
      return (
        axios({
          url: `${BASE_URL}/api/login`,
          headers: {
            'Content-Type': 'application/json'
          },
          data: submitData,
          method: 'post'
        })
          .then(response => {
            // console.log('token print----- : ', response.data)
            setLoginResponse(response.data)
            // localStorage.setItem('token', response.data.token);
            if (response.data.success && response.data?.token) {
              Cookies.set('cToken', response.data.token)
              Cookies.set('cRole', response.data.currentUser.role)
              Cookies.set('cName', response.data.currentUser.name)
              Cookies.set('cEmail', response.data.currentUser.email)
              if (response.data.currentUser.role === 'warehouse') {
                return Router.push('/admin/verify')
              } else return Router.push('/admin/dealers')
            }
          })
          // .then((json) => ({
          //   type: 'SUCCESS',
          //   payload: json,
          // }))
          .catch(err => {
            // if (getToken() && err && err.response && err.response.status === 401) {
            //   logOut()
            // } else {
            //   return {
            //     type: 'FAIL',
            //   }
            // }
            console.log('token print error ----- : ', err)
          })
      )
    }

  }

  const useStyles = makeStyles(styles)
  const classes = useStyles()
  return (
    <div>
      <GridContainer justify='center'>
        <GridItem xs={10} sm={6} md={4}>
          <Card profile style={{ marginTop: '140px' }}>
            <CardAvatar profile>
              <a href='#pablo' onClick={e => e.preventDefault()}>
                <img src={tcb_logo} alt='TCB logo' />
              </a>
            </CardAvatar>
            <CardBody profile>
              {/* <h6 className={classes.cardCategory}>CEO / CO-FOUNDER</h6> */}
              <h4
                style={{ fontWeight: 'bold', marginBottom: 0 }}
                className={classes.cardTitle}
              >
                Trading Corporation of Bangladesh
              </h4>
              <p className={classes.description}>Truck Sell Digitalization</p>{' '}
              <br />
              <form onSubmit={submitLogin}>
                {console.log('errors---', errors)}
                <FormControl
                  // className={clsx(classes.margin, classes.textField)}
                  variant='outlined'
                >
                  <TextField
                    style={{ width: '239px' }}
                    id='outlined-basic'
                    label='Email'
                    variant='outlined'
                    onChange={e => {
                      setEmail(e.target.value)
                      handleInputValue(e)
                    }}
                    onBlur={handleInputValue}
                    name='email'
                    error={errors['email']}
                    {...(errors['email'] && {
                      error: true,
                      helperText: errors['email']
                    })}
                  />{' '}
                  {loginResponse?.error?.email && !errors['email'] ? (
                    <p className={classes.error_txt}>
                      {loginResponse?.error?.email[0]}
                    </p>
                  ) : ''}
                  <br />
                  {/* <TextField
                id='outlined-basic'
                type='password'
                label='Password'
                variant='outlined'
                onChange={e => setPassword(e.target.value)}
              /> */}
                </FormControl>
                <FormControl
                  // className={clsx(classes.margin, classes.textField)}
                  variant='outlined'
                >
                  <InputLabel htmlFor='outlined-adornment-password'>
                    Password
                  </InputLabel>
                  <OutlinedInput
                    id='outlined-adornment-password'
                    type={values.showPassword ? 'text' : 'password'}
                    value={password}
                    onChange={e => {
                      setPassword(e.target.value)
                      handleInputValue(e)
                    }}
                    endAdornment={
                      <InputAdornment position='end'>
                        <IconButton
                          aria-label='toggle password visibility'
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge='end'
                        >
                          {values.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                    labelWidth={70}
                    onBlur={handleInputValue}
                    name='password'
                    // error={errors['password']}
                    // {...(errors['password'] && {
                    //   error: true,
                    //   helperText: errors['password']
                    // })}
                  />
                  {errors['password'] && (
                    <p className={classes.error_txt}>{errors['password']}</p>
                  )}
                  {loginResponse?.message && !errors['password'] ? (
                    <p className={classes.error_txt}>
                      {loginResponse?.message}
                    </p>
                  ) : ''}

                  {loginResponse?.error?.password && !errors['password'] ? (
                    <p className={classes.error_txt}>
                      {loginResponse?.error?.password[0]}
                    </p>
                  ) : ''}
                </FormControl>
                <br /> <br />
                <Button
                  type='submit'
                  color='primary'
                  round
                  style={{ background: '#1BA64F' }}
                  disabled={errors['email'] || errors['password']}
                  // onClick={() => submitLogin()}
                >
                  Log In
                </Button>
              </form>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  )
}

// UserProfile.layout = Admin;

export default UserProfile
