import React, { useState, useEffect } from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import CustomInput from 'components/CustomInput/CustomInput.js'
import Button from 'components/CustomButtons/Button.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardAvatar from 'components/Card/CardAvatar.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Link from 'next/link'

import avatar from 'assets/img/faces/marc.jpg'

import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import axios from 'axios'
import Router, { withRouter } from 'next/router'
import { useRouter } from 'next/router'
import Input from '@material-ui/core/Input'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import TextareaAutosize from '@material-ui/core/TextareaAutosize'

import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import SaveIcon from '@material-ui/icons/Save'
import CancelIcon from '@material-ui/icons/Cancel'
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate'

import { districtList, upazilaList } from './constData.json'
import { useAccountFormControls } from '../../components/AccountFormControls/AccountFormControls'

import { withStyles } from '@material-ui/core/styles'
import { green } from '@material-ui/core/colors'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import Favorite from '@material-ui/icons/Favorite'
import FavoriteBorder from '@material-ui/icons/FavoriteBorder'
import FormControl from '@material-ui/core/FormControl'
import SendIcon from '@material-ui/icons/Send'
import WithAuth from '../../components/WithAuth'

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600]
    }
  },
  checked: {}
})(props => <Checkbox color='default' {...props} />)

const useStyles = makeStyles(theme => ({
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0'
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  },
  hide_change_pass: {
    display: 'none !important'
  }
}))

function Account () {
  const {
    handleInputValue,
    handleFormSubmit,
    formIsValid,
    errors,
    values
  } = useAccountFormControls()

  const classes = useStyles()

  const [open, setOpen] = React.useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  useEffect(() => {
    // districtList.map(item => (
    //   <MenuItem value={item.name} key={item.id}>
    //     {item.name}
    //   </MenuItem>

    // ))

    districtList.forEach(item => {
      if (item.name == values.district) {
        setGenerateDealerId(`01${item.districtCode}`)
      }
    })
  }, [values.district])

  useEffect(() => {
    // {upazilaList.map(item =>
    //   item.district == values.district
    //     ? item.upazilas.map(itemUpz => (
    //         <MenuItem value={itemUpz.name} key={itemUpz.id}>
    //           {itemUpz.name}
    //         </MenuItem>
    //       ))
    //     : null
    // )}

    upazilaList.forEach(item => {
      if (item.district == values.district) {
        item.upazilas.forEach(upazila => {
          if (upazila.name == values.upazila) {
            setGenerateDealerId(`01${item.districtCode}${upazila.upazilaCode}`)

            dealerIdGeneration(`01${item.districtCode}${upazila.upazilaCode}`)
          }
        })
      }
    })
  }, [values.upazila])

  useEffect(() => {
    return (
      axios({
        url: `${BASE_URL}/api/get_user`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: {
          token: Cookies.get('cToken'),
          userId: query?.id
        },
        method: 'post'
      })
        .then(response => {
          if (response.data) {
            console.log('user print----- : ', response.data)

            // setCurrentUser(response.data.user)
            setUserUpdateCheck(response.data.user)

            setUserName(response.data.user?.name)
            setUserEmail(response.data.user?.email)
            setUserZone(response.data.user?.zone)
            setUserRole(response.data.user?.role)
          } else {
            console.log('success false---', response.data)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('error print----- : ', err)
        })
    )
  }, [])

  const { query } = useRouter()

  const [name, setName] = useState()
  const [newPass, setNewPass] = useState()
  const [confirmNewPass, setConfirmNewPass] = useState()
  const [mobileNo, setMobileNo] = useState()
  const [zone, setZone] = useState('Dhaka')
  const [district, setDistrict] = useState()
  const [upazila, setUpazila] = useState()
  const [distanceZonal, setDistanceZonal] = useState()
  const [shopAddress, setShopAddress] = useState()
  const [contractStatus, setContractStatus] = useState()
  const [relationType, setRelationType] = useState('son')
  const [contractRenewalDate, setContractRenewalDate] = useState(null)
  const [familyDealerId, setFamilyDealerId] = useState()
  const [limitNumber, setLimitNumber] = useState()
  const [generateDealerId, setGenerateDealerId] = useState()
  const [familyDealerName, setFamilyDealerName] = useState()
  const [currentUser, setCurrentUser] = useState()
  const [userName, setUserName] = useState()
  const [userEmail, setUserEmail] = useState()
  const [userZone, setUserZone] = useState()
  const [userRole, setUserRole] = useState()
  const [userUpdateCheck, setUserUpdateCheck] = useState()
  const [toggleChangePass, setToggleChangePass] = useState(true)
  const [currentPassMatch, setCurrentPassMatch] = useState(false)
  const [isUpdatedPass, setIsUpdatedPass] = useState(false)
  const [isUpdatedAccountDetails, setIsUpdatedAccountDetails] = useState(false)
  const [statusComment, setStatusComment] = useState('')
  const [selectedImage, setSelectedImage] = useState()
  const [isOverSizeImage, setIsOverSizeImage] = useState(false)
  const [imageDimensions, setImageDimensions] = useState({})

  const storeDealer = () => {
    const submitData = {
      dealer_id: generateDealerId,
      name: values.dealerName,
      shop_name: values.shopName,
      distance_from_zonal: values.distanceZonal,
      shop_address: values.shopAddress,
      mobile_no: values.mobileNo,
      zone: zone,
      district: values.district,
      upazila: values.upazila,
      contract_status: values.contractStatus,
      show_cause: state.checkedA,
      forgery_proved: state.checkedB,
      flagged: state.checkedC,
      comment: statusComment,
      contract_date: values.contractDate,
      contract_expiry_date: values.contractExpiryDate,
      ...(contractRenewalDate && {
        contract_renewal_date: contractRenewalDate
      }),
      family_dealer_id: values.familyDealerId,
      family_relation_type: values.relationType,
      selectedImage: selectedImage
    }

    const bodyFormData = new FormData()

    bodyFormData.append('dealer_id', generateDealerId)
    bodyFormData.append('name', values.dealerName)
    bodyFormData.append('shop_name', values.shopName)
    bodyFormData.append('distance_from_zonal', values.distanceZonal)
    bodyFormData.append('shop_address', values.shopAddress)
    bodyFormData.append('mobile_no', values.mobileNo)
    bodyFormData.append('zone', zone)
    bodyFormData.append('district', values.district)
    bodyFormData.append('upazila', values.upazila)
    bodyFormData.append('contract_status', values.contractStatus)
    bodyFormData.append('show_cause', state.checkedA ? 1 : 0)
    bodyFormData.append('forgery_proved', state.checkedB ? 1 : 0)
    bodyFormData.append('flagged', state.checkedC ? 1 : 0)
    bodyFormData.append('comment', statusComment)
    bodyFormData.append('contract_date', values.contractDate)
    bodyFormData.append('contract_expiry_date', values.contractExpiryDate)
    contractRenewalDate &&
      bodyFormData.append('contract_renewal_date', contractRenewalDate)
    bodyFormData.append('family_dealer_id', values.familyDealerId)
    bodyFormData.append('family_relation_type', values.relationType)
    bodyFormData.append('selectedImage', selectedImage)

    return (
      axios({
        url: `${BASE_URL}/api/create`,
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: bodyFormData,
        method: 'post'
      })
        .then(response => {
          console.log('token print----- : ', response)
          return Router.push(
            `/admin/dealers?searchDealer=${query.searchDealer}&page=${query.page}`
          )
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const sendSmsDealer = () => {
    return (
      axios({
        url: `${BASE_URL}/api/send-sms?phone=${values.mobileNo}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          console.log('sms resp----', response)
          return response
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const dealerIdGeneration = partialId => {
    return (
      axios({
        url: `${BASE_URL}/api/last-dealer-id/${partialId}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          if (response.data.success) {
            let dealerIdData = response.data.data + 1

            console.log(
              'last dealerID print-----ff : ',
              dealerIdData.toString().charAt(0)
            )

            if (dealerIdData.toString().charAt(0) == 0) {
              setGenerateDealerId(`${dealerIdData}`)
            } else {
              setGenerateDealerId(`0${dealerIdData}`)
            }
          } else {
            console.log('success false---', response.data.success)

            setGenerateDealerId(`${partialId}001`)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const currentPassCheck = CurrentPass => {
    const submitData = {
      token: Cookies.get('cToken'),
      password: CurrentPass
    }
    return (
      axios({
        url: `${BASE_URL}/api/current-password-check`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: submitData,
        method: 'post'
      })
        .then(response => {
          console.log('current-password-check print----- : ', response.data)
          setCurrentPassMatch(response.data.isCurrentPass)
          // localStorage.setItem('token', response.data.token);
          // if (response.data.success && response.data?.token) {
          //   Cookies.set('cToken', response.data.token)
          //   Cookies.set('cRole', response.data.currentUser.role)
          //   return Router.push('/admin/dealers')
          // }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('current-password-check print-  error ----- : ', err)
        })
    )
  }
  const updateUser = () => {
    const submitData = {
      token: Cookies.get('cToken'),
      userId: query?.id,
      name: userName,
      email: userEmail,
      zone: userZone,
      password: newPass,
      role: userRole
    }
    return (
      axios({
        url: `${BASE_URL}/api/update-user`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: submitData,
        method: 'post'
      })
        .then(response => {
          console.log('password-change print----- : ', response.data)
          setIsUpdatedPass(response.data.isUpdatedPass)
          if (response.data.isUpdatedPass) {
            setToggleChangePass(true)
          }
          // localStorage.setItem('token', response.data.token);
          // if (response.data.success && response.data?.token) {
          //   Cookies.set('cToken', response.data.token)
          //   Cookies.set('cRole', response.data.currentUser.role)
          //   return Router.push('/admin/dealers')
          // }

          return Router.push('/admin/users')
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('password-change print-  error ----- : ', err)
        })
    )
  }

  // for checkbox----

  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
    checkedC: false,
    checkedG: false
  })

  // const [checkShowCause, setCheckShowCause] = useState(true)

  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked })
  }

  // end for checkbox----

  return (
    <Admin>
      <GridContainer>
        <div>
          <Modal
            aria-labelledby='transition-modal-title'
            aria-describedby='transition-modal-description'
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <h2 id='transition-modal-title'>Confirm?</h2>
                <p id='transition-modal-description'>
                  Save all the information & add the dealer.
                </p>
                <div
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <Button
                    variant='contained'
                    color='secondary'
                    // className={classes.button}
                    startIcon={<CancelIcon />}
                    onClick={() => handleClose()}
                  >
                    Cancel
                  </Button>
                  <Button
                    variant='contained'
                    color='secondary'
                    // className={classes.button}
                    style={{ background: '#00AC34', marginLeft: '20px' }}
                    startIcon={<SaveIcon />}
                    onClick={() => storeDealer()}
                  >
                    Save
                  </Button>
                </div>
                <Button
                  fullWidth
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  style={{
                    background: 'transparent',
                    textTransform: 'none',
                    color: '#00AC34',
                    border: '1px solid #00AC34',
                    marginTop: '15px'
                  }}
                  startIcon={<SendIcon />}
                  onClick={() => {
                    sendSmsDealer()
                    storeDealer()
                  }}
                >
                  Send Confirmation SMS & save
                </Button>
              </div>
            </Fade>
          </Modal>
        </div>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color='primary' style={{ background: '#4CAF50' }}>
              <h4 className={classes.cardTitleWhite}>Account details</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              {/* <h4 style={{ marginBottom: '10px' }}>Dealer Id : {generateDealerId}</h4> */}
              {/* <br />
              <br /> */}

              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Full name'
                    variant='outlined'
                    value={userName}
                    fullWidth
                    onChange={e => setUserName(e.target.value)}
                    disabled={
                      Cookies.get('cRole') === 'superadmin' ? false : true
                    }
                    // onBlur={handleInputValue}
                    name='dealerName'
                    error={errors['dealerName']}
                    {...(errors['dealerName'] && {
                      error: true,
                      helperText: errors['dealerName']
                    })}
                    InputLabelProps={{ shrink: true }}
                  />{' '}
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Email'
                    variant='outlined'
                    value={userEmail}
                    fullWidth
                    onChange={e => setUserEmail(e.target.value)}
                    disabled={
                      Cookies.get('cRole') === 'superadmin' ? false : true
                    }
                    // onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='email'
                    error={errors['shopName']}
                    {...(errors['shopName'] && {
                      error: true,
                      helperText: errors['shopName']
                    })}
                    InputLabelProps={{ shrink: true }}
                  />{' '}
                  <br /> <br />
                </GridItem>

                <GridItem xs={12} sm={12} md={4}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Zone
                    </InputLabel>
                    <Select
                      label='Zone'
                      fullWidth
                      value={`${userZone}`}
                      disabled={
                        Cookies.get('cRole') === 'superadmin' ? false : true
                      }
                      onChange={e => setUserZone(e.target.value)}
                      // onChange={handleInputValue}
                      // onBlur={handleInputValue}
                      name='zone'
                      // error={errors['distanceZonal']}
                      // {...(errors['distanceZonal'] && {
                      //   error: true,
                      //   helperText: errors['distanceZonal']
                      // })}
                      // inputProps={{
                      //   name: 'distanceZonal',
                      //   id: 'outlined-age-native-simple'
                      // }}
                    >
                      <MenuItem value={'Dhaka'}>Dhaka</MenuItem>
                      <MenuItem value={'Rajshahi'}>Rajshahi</MenuItem>
                      <MenuItem value={'Chattogram'}>Chattogram</MenuItem>
                      <MenuItem value={'Khulna'}>Khulna</MenuItem>
                      <MenuItem value={'Barishal'}>Barishal</MenuItem>
                      <MenuItem value={'Moulvibazar'}>Moulvibazar</MenuItem>
                      <MenuItem value={'Rangpur'}>Rangpur</MenuItem>
                      <MenuItem value={'Mymensingh'}>
                        Mymensingh (Camp)
                      </MenuItem>
                    </Select>
                  </FormControl>
                </GridItem>

                {Cookies.get('cRole') === 'superadmin' && (
                  <GridItem xs={12} sm={12} md={4}>
                    <FormControl
                      fullWidth
                      variant='outlined'
                      className={classes.formControl}
                    >
                      <InputLabel
                        style={{ marginBottom: '10px' }}
                        htmlFor='outlined-age-simple'
                      >
                        Role
                      </InputLabel>
                      <Select
                        label='Role'
                        fullWidth
                        value={`${userRole}`}
                        disabled={
                          Cookies.get('cRole') === 'superadmin' ? false : true
                        }
                        onChange={e => setUserRole(e.target.value)}
                        // onChange={handleInputValue}
                        // onBlur={handleInputValue}
                        name='userRole'
                        // error={errors['distanceZonal']}
                        // {...(errors['distanceZonal'] && {
                        //   error: true,
                        //   helperText: errors['distanceZonal']
                        // })}
                        // inputProps={{
                        //   name: 'distanceZonal',
                        //   id: 'outlined-age-native-simple'
                        // }}
                      >
                        <MenuItem value={'admin'}>Admin</MenuItem>
                        <MenuItem value={'warehouse'}>Warehouse</MenuItem>
                      </Select>
                    </FormControl>
                  </GridItem>
                )}

                {/* <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    InputLabelProps={{ shrink: true }}
                    id='standard-basic'
                    label='Zone'
                    variant='outlined'
                    value={userZone}
                    disabled
                    fullWidth
                    onChange={e => setUserZone(e.target.value)}
                  />{' '}
                  <br /> <br />
                </GridItem> */}
              </GridContainer>

              {/* <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel style={{ color: "#AAAAAA" }}>About me</InputLabel>
                  <CustomInput
                    labelText="Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo."
                    id="about-me"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      multiline: true,
                      rows: 5,
                    }}
                  />
                </GridItem>
              </GridContainer> */}
            </CardBody>
            <CardFooter>
              {/* <Button
                type='submit'
                color='primary'
                // onClick={() => storeDealer()}
                onClick={handleOpen}
                style={{ background: '#4CAF50' }}
              >
                Save dealer
              </Button> */}
            </CardFooter>
          </Card>
        </GridItem>

        {/* start Family relation section */}
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader
              plain
              color='primary'
              style={{
                background: '#898b8a',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Password</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <br />
              <div className={!toggleChangePass && classes.hide_change_pass}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <div className={!toggleChangePass && classes.hide_change_pass}>
                      <TextField
                        id='standard-basic'
                        type='password'
                        label='Current Password'
                        variant='outlined'
                        value={'some value'}
                        disabled
                        fullWidth
                        // onChange={e => setShopName(e.target.value)}
                        // onChange={handleInputValue}
                        // onBlur={handleInputValue}
                        name='shopName'
                        error={errors['shopName']}
                        {...(errors['shopName'] && {
                          error: true,
                          helperText: errors['shopName']
                        })}
                      />{' '}
                    </div>
                    <br /> <br />
                  </GridItem>
                  <div className={!toggleChangePass && classes.hide_change_pass}>
                    <Button
                      className={!toggleChangePass && classes.hide_change_pass}
                      type='button'
                      color='primary'
                      onClick={() => setToggleChangePass(!toggleChangePass)}
                      // onClick={handleOpen}
                      style={{
                        background: '#4CAF50',
                        height: '55px',
                        margin: '0px'
                      }}
                      // disabled={!formIsValid() || !selectedImage ? true : false}
                    >
                      Change
                    </Button>
                  </div>
                </GridContainer>
              </div>
              <GridContainer>
                {console.log('toggleChangePass--', toggleChangePass)}
                {/* <h1 className={toggleChangePass && classes.hide_change_pass}>hide_change_pass</h1> */}
                <GridItem xs={12} sm={12} md={3}>
                  <div className={toggleChangePass && classes.hide_change_pass}>
                    <TextField
                      id='standard-basic'
                      type='password'
                      label='Current Password'
                      variant='outlined'
                      fullWidth
                      // onChange={e => setShopName(e.target.value)}
                      // onChange={handleInputValue}
                      onChange={e => currentPassCheck(e.target.value)}
                      // onBlur={handleInputValue}
                      onBlur={e => currentPassCheck(e.target.value)}
                      name='shopName'
                      // error={errors['shopName']}
                      // {...(errors['shopName'] && {
                      //   error: true,
                      //   helperText: errors['shopName']
                      // })}
                      error={!currentPassMatch}
                    />{' '}
                  </div>
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <div className={toggleChangePass && classes.hide_change_pass}>
                    <TextField
                      id='standard-basic'
                      type='password'
                      label='New Password'
                      variant='outlined'
                      fullWidth
                      onChange={e => {
                        setNewPass(e.target.value)
                        handleInputValue(e)
                      }}
                      // onChange={handleInputValue}
                      onBlur={handleInputValue}
                      name='password'
                      disabled={!currentPassMatch}
                      error={errors['password']}
                      {...(errors['password'] && {
                        error: true,
                        helperText: errors['password']
                      })}
                    />{' '}
                  </div>
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <div className={toggleChangePass && classes.hide_change_pass}>
                    <TextField
                      id='standard-basic'
                      type='password'
                      label='Confirm new Password'
                      variant='outlined'
                      fullWidth
                      onChange={e => setConfirmNewPass(e.target.value)}
                      // onChange={handleInputValue}
                      onBlur={handleInputValue}
                      name='shopName'
                      disabled={!currentPassMatch}
                      error={confirmNewPass && newPass != confirmNewPass}
                      {...(confirmNewPass &&
                        newPass != confirmNewPass && {
                          error: true,
                          helperText: 'Password do not match.'
                        })}
                    />{' '}
                  </div>
                  <br /> <br />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </GridItem>

        {Cookies.get('cRole') === 'superadmin' ? (
          <GridItem xs={12} sm={12} md={12} style={{ marginTop: '20px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Button
                type='submit'
                color='primary'
                // onClick={() => storeDealer()}
                onClick={() => updateUser()}
                style={{ background: '#4CAF50' }}
                disabled={
                  (newPass && newPass === confirmNewPass) ||
                  (userEmail != userUpdateCheck?.email &&
                    newPass === confirmNewPass) ||
                  (userName != userUpdateCheck?.name &&
                    newPass === confirmNewPass) ||
                  (userZone != userUpdateCheck?.zone &&
                    newPass === confirmNewPass) ||
                  (userRole != userUpdateCheck?.role &&
                    newPass === confirmNewPass)
                    ? false
                    : true
                }
              >
                Save
              </Button>

              <Button
                className={toggleChangePass && classes.hide_change_pass}
                color='primary'
                style={{ background: '#898B8A' }}
                onClick={() => setToggleChangePass(true)}
              >
                Cancel
              </Button>
            </div>
          </GridItem>
        ) : (
          <GridItem xs={12} sm={12} md={12} style={{ marginTop: '20px' }}>
            <div
              style={{ display: 'flex', justifyContent: 'space-between' }}
              className={toggleChangePass && classes.hide_change_pass}
            >
              <Button
                type='submit'
                color='primary'
                // onClick={() => storeDealer()}
                onClick={() => updateUser()}
                style={{ background: '#4CAF50' }}
                disabled={
                  (newPass && newPass === confirmNewPass) ||
                  (userEmail != userUpdateCheck?.email &&
                    newPass === confirmNewPass) ||
                  (userName != userUpdateCheck?.name &&
                    newPass === confirmNewPass) ||
                  (userZone != userUpdateCheck?.zone &&
                    newPass === confirmNewPass) ||
                  (userRole != userUpdateCheck?.role &&
                    newPass === confirmNewPass)
                    ? false
                    : true
                }
              >
                Save
              </Button>
              <Button
                color='primary'
                style={{ background: '#898B8A' }}
                onClick={() => setToggleChangePass(true)}
              >
                Cancel
              </Button>
            </div>
          </GridItem>
        )}
      </GridContainer>
    </Admin>
  )
}

// Account.layout = Admin

export default WithAuth(Account, ['superadmin', 'admin', 'warehouse'])
