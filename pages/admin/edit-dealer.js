import React, { useState, useEffect } from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import CustomInput from 'components/CustomInput/CustomInput.js'
import Button from 'components/CustomButtons/Button.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardAvatar from 'components/Card/CardAvatar.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Link from 'next/link'

import avatar from 'assets/img/faces/marc.jpg'

import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import axios from 'axios'
import Router, { withRouter } from 'next/router'
import { useRouter } from 'next/router'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import { districtList, upazilaList } from './constData.json'
import { useFormControls } from '../../components/ContactFormControls/DealerContactFormControls'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import SaveIcon from '@material-ui/icons/Save'
import CancelIcon from '@material-ui/icons/Cancel'
import Input from '@material-ui/core/Input'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import FormControl from '@material-ui/core/FormControl'
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import stylesCustomAllotment from './add-new-allotment.module.css'
import Chip from '@material-ui/core/Chip'

const useStyles = makeStyles(theme => ({
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0'
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

function UserProfile () {
  const {
    handleInputValue,
    handleFormSubmit,
    formIsValid,
    handlePresetValue,
    errors,
    values
  } = useFormControls()

  const [open, setOpen] = React.useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  let serial = 0

  const classes = useStyles()

  const [name, setName] = useState()
  const [shopName, setShopName] = useState()
  const [mobileNo, setMobileNo] = useState()
  const [zone, setZone] = useState()
  const [district, setDistrict] = useState()
  const [upazila, setUpazila] = useState()
  const [distanceZonal, setDistanceZonal] = useState()
  const [shopAddress, setShopAddress] = useState()
  const [contractStatus, setContractStatus] = useState()
  const [relationType, setRelationType] = useState()
  const [contractDate, setContractDate] = useState()
  const [contractRenewalDate, setContractRenewalDate] = useState()
  const [contractExpiryDate, setContractExpiryDate] = useState()
  const [generateDealerId, setGenerateDealerId] = useState()
  const [familyDealerName, setFamilyDealerName] = useState()
  const [statusComment, setStatusComment] = useState()
  const [familyDealerId, setFamilyDealerId] = useState()
  const [selectedImage, setSelectedImage] = useState()
  const [getImage, setGetImage] = useState()
  const [imageDimensions, setImageDimensions] = useState({})
  const [isOverSizeImage, setIsOverSizeImage] = useState(false)
  const [allotmentList, setAllotmentList] = useState([])

  const [dealerSingle, setDealerSingle] = useState({
    name: '',
    mobile_no: '',
    district: '',
    distance_from_zonal: '',
    contract_status: '',
    shop_address: '',
    shop_name: '',
    upazila: '',
    zone: ''
  })

  // const [shopName, setShopName] = useState();
  // const [shopName, setShopName] = useState();

  const { query } = useRouter()
  let susam = {}

  // useEffect(() => {
  //   districtList.forEach(item => {
  //     if (item.name == district) {
  //       setGenerateDealerId(`01${item.districtCode}`)
  //     }
  //   })
  // }, [district])

  const districtCodeID = () => {
    districtList.forEach(item => {
      if (item.name == district) {
        setGenerateDealerId(`01${item.districtCode}`)
      }
    })
  }

  useEffect(() => {
    return (
      axios({
        url: `${BASE_URL}/api/dealer/${familyDealerId}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          if (response.data) {
            let family_dealer_name = response.data.name

            console.log('dealerName print----- : ', family_dealer_name)

            setFamilyDealerName(family_dealer_name)
          } else {
            console.log('success false---', response.data.success)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('error print----- : ', err)
        })
    )
  }, [familyDealerId])

  // useEffect(() => {
  //   upazilaList.forEach(item => {
  //     if (item.district == district) {
  //       item.upazilas.forEach(item_upazila => {
  //         if (item_upazila.name == upazila) {
  //           setGenerateDealerId(
  //             `01${item.districtCode}${item_upazila.upazilaCode}`
  //           )

  //           dealerIdGeneration(
  //             `01${item.districtCode}${item_upazila.upazilaCode}`
  //           )
  //         }
  //       })
  //     }
  //   })
  // }, [upazila])

  const upazilaCodeID = () => {
    upazilaList.forEach(item => {
      if (item.district == district) {
        item.upazilas.forEach(item_upazila => {
          if (item_upazila.name == upazila) {
            setGenerateDealerId(
              `01${item.districtCode}${item_upazila.upazilaCode}`
            )

            dealerIdGeneration(
              `01${item.districtCode}${item_upazila.upazilaCode}`
            )
          }
        })
      }
    })
  }

  useEffect(() => {
    let dealerData = axios({
      url: `${BASE_URL}/api/dealers/${query.id}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        console.log('Single from use effect print----- : ', response.data)
        // setName(response.data.name)
        let bufferAllotment = []

        // console.log(dealerSingle)
        let data = response.data
        // console.log(susam.name, '----susam---')

        // setDealerSingle({
        //   name: response.data.name,
        //   mobile_no: response.data.mobile_no,
        //   district: response.data.district,
        //   distance_from_zonal: response.data.distance_from_zonal,
        //   contract_status: response.data.contract_status,
        //   shop_address: response.data.shop_address,
        //   shop_name: response.data.shop_name,
        //   upazila: response.data.upazila,
        //   zone: response.data.zone
        // })

        setGenerateDealerId(data.dealer_id)
        setName(response.data.name)
        setShopName(data.shop_name)
        setMobileNo(data.mobile_no)
        setZone(data.zone)
        setDistrict(data.district)
        setUpazila(data.upazila)
        setDistanceZonal(data.distance_from_zonal)
        setShopAddress(data.shop_address)
        setContractStatus(data.contract_status)
        setContractDate(data.contract_date)
        setContractRenewalDate(data.contract_renewal_date)
        setContractExpiryDate(data.contract_expiry_date)
        setFamilyDealerId(data.family_dealer_id)
        setRelationType(data.family_relation_type)
        setStatusComment(data.comment)
        setCheckShowCause(data.show_cause)
        setCheckForgery(data.forgery_proved)
        setCheckFlagged(data.flagged)

        setGetImage(data.image_name)

        response.data.allotments.map(allotment => {
          if (allotment.allotment) {
            bufferAllotment.push({
              allotment_no: allotment.allotment_id,
              allotment_date: allotment.allotment.allotment_date,
              allotment_status: allotment.is_released,
              payment: allotment.payment_status,
              warehouse: allotment.is_released
            })
          }
        })
        setAllotmentList(bufferAllotment)

        // values.dealerName=data.name;

        // setGenerateDealerId(data.dealer_id);
        // handlePresetValue('shopName', data.shop_name)
        // setTimeout(handlePresetValue('shopAddress', data.shop_address), 1000)
        // setTimeout(handlePresetValue('mobileNo', data.mobile_no), 1000)
        // handlePresetValue('district', data.district)
        // handlePresetValue('upazila', data.upazila)
        // handlePresetValue('distanceZonal', data.distance_from_zonal)
        // handlePresetValue('contractStatus', data.contract_status)

        // handlePresetValue(data)

        return response.data
        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('single token print----- : ', err)
      })
  }, [])

  const updateDealer = () => {
    const submitData = {
      dealer_id: generateDealerId,
      name: name,
      shop_name: shopName,
      distance_from_zonal: distanceZonal,
      shop_address: shopAddress,
      mobile_no: mobileNo,
      zone: zone,
      district: district,
      upazila: upazila,
      contract_status: contractStatus,
      show_cause: checkShowCause,
      forgery_proved: checkForgery,
      flagged: checkFlagged,
      comment: statusComment,
      contract_date: contractDate,
      contract_expiry_date: contractExpiryDate,
      contract_renewal_date: contractRenewalDate,
      family_dealer_id: familyDealerId,
      family_relation_type: relationType,
      // selectedImage: selectedImage
    }

    // const bodyFormData = new FormData()

    // bodyFormData.append('dealer_id', generateDealerId)
    // bodyFormData.append('name', name)
    // bodyFormData.append('shop_name', shopName)
    // bodyFormData.append('distance_from_zonal', distanceZonal)
    // bodyFormData.append('shop_address', shopAddress)
    // bodyFormData.append('mobile_no', mobileNo)
    // bodyFormData.append('zone', zone)
    // bodyFormData.append('district', district)
    // bodyFormData.append('upazila', upazila)
    // bodyFormData.append('contract_status', contractStatus)
    // bodyFormData.append('show_cause', checkShowCause ? 1 : 0)
    // bodyFormData.append('forgery_proved', checkForgery ? 1 : 0)
    // bodyFormData.append('flagged', checkFlagged ? 1 : 0)
    // bodyFormData.append('comment', statusComment)
    // bodyFormData.append('contract_date', contractDate)
    // bodyFormData.append('contract_expiry_date', contractExpiryDate)
    // contractRenewalDate && bodyFormData.append('contract_renewal_date', contractRenewalDate)
    // bodyFormData.append('family_dealer_id', familyDealerId)
    // bodyFormData.append('family_relation_type', relationType)
    // bodyFormData.append('selectedImage', selectedImage)

    return (
      axios({
        url: `${BASE_URL}/api/update/${query.id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: submitData,
        method: 'put'
      })
        .then(response => {
          console.log('update print----- : ', response.data)
          return Router.push(`/admin/dealers?searchDealer=${query.searchDealer}&page=${query.page}`)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const dealerIdGeneration = partialId => {
    return (
      axios({
        url: `${BASE_URL}/api/last-dealer-id/${partialId}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          if (response.data.success) {
            let dealerIdData = response.data.data + 1

            console.log(
              'last dealerID print-----ff : ',
              dealerIdData.toString().charAt(0)
            )

            if (dealerIdData.toString().charAt(0) == 0) {
              setGenerateDealerId(`${dealerIdData}`)
            } else {
              setGenerateDealerId(`0${dealerIdData}`)
            }
          } else {
            console.log('success false---', response.data.success)

            setGenerateDealerId(`${partialId}001`)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const assignAllotmentList = () => {
    return (
      <>
        {allotmentList.map(item => (
          <tr>
            <td>{++serial}</td>
            <td>{item.allotment_no}</td>
            <td>{item.allotment_date}</td>
            <td>
              {item.allotment_status ? (
                <Chip
                  style={{ color: '#4CAF50' }}
                  icon={<FiberManualRecordIcon style={{ color: '#4CAF50' }} />}
                  label='FULFILLED'
                />
              ) : (
                <Chip icon={<FiberManualRecordIcon />} label='PENDING' />
              )}
            </td>
            <td>{item.payment ? 'Verified' : 'Pending'}</td>
            <td>{item.warehouse ? 'Cleared' : '-'}</td>
          </tr>
        ))}
      </>
    )
  }

  const getDealerName = () => {
    return (
      axios({
        url: `${BASE_URL}/api/dealer/${familyDealerId}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          if (response.data) {
            let family_dealer_name = response.data.name

            console.log('dealerName print----- : ', family_dealer_name)

            setFamilyDealerName(family_dealer_name)
          } else {
            console.log('success false---', response.data.success)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('error print----- : ', err)
        })
    )
  }

  // for checkbox----

  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
    checkedC: false,
    checkedG: false
  })

  const [checkShowCause, setCheckShowCause] = useState(false)
  const [checkForgery, setCheckForgery] = useState(false)
  const [checkFlagged, setCheckFlagged] = useState(false)

  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked })
  }

  // end for checkbox----

  return (
    <div>
      <GridContainer>
        <div>
          {console.log('form values------ :', values)}
          <Modal
            aria-labelledby='transition-modal-title'
            aria-describedby='transition-modal-description'
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <h2 id='transition-modal-title'>Confirm?</h2>
                <p id='transition-modal-description'>
                  Save all the information & add the dealer.
                </p>
                <Button
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  startIcon={<CancelIcon />}
                  onClick={() => handleClose()}
                >
                  Cancel
                </Button>
                <Button
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  style={{ background: '#00AC34', marginLeft: '20px' }}
                  startIcon={<SaveIcon />}
                  onClick={() => updateDealer()}
                >
                  Save
                </Button>
              </div>
            </Fade>
          </Modal>
        </div>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color='primary' style={{ background: '#4CAF50' }}>
              <h4 className={classes.cardTitleWhite}>Edit Dealer</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              {/* <h4 style={{ marginBottom: '10px' }}>Dealer Id : {generateDealerId}</h4> */}
              {/* <br />
              <br /> */}
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <div
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <div>
                      {selectedImage ? (
                        <div>
                          <img
                            src={URL.createObjectURL(selectedImage)}
                            style={{
                              width: '150px',
                              height: '150px',
                              marginBottom: '20px',
                              borderRadius: '50%'
                            }}
                            alt='Thumb'
                          />
                        </div>
                      ) : (
                        getImage && (
                          <div>
                            <img
                              // src={URL.createObjectURL(selectedImage)}
                              src={`${BASE_URL}/images/dealer/${getImage}`}
                              style={{
                                width: '150px',
                                height: '150px',
                                marginBottom: '20px',
                                borderRadius: '50%'
                              }}
                              alt='Thumb'
                            />
                          </div>
                        )
                      )}
                    </div>
                    <div
                      style={{
                        width: '110px',
                        height: '80px',
                        background: '#B2E6C2',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right',
                        // marginRight: '20px',
                        padding: '10px'
                      }}
                      className='show_cause_box'
                    >
                      <p>Rating will appear here</p>
                    </div>
                  </div>
                </GridItem>

                <GridItem xs={12} sm={12} md={12}>
                  <div style={{ display: 'flex', justifyContent: 'right' }}>
                    {checkShowCause ? (
                      <div
                        style={{
                          width: '20px',
                          height: '20px',
                          background: '#EA80FC',
                          marginBottom: '20px',
                          borderRadius: '2px',
                          float: 'right',
                          marginRight: '20px'
                        }}
                        className='show_cause_box'
                      ></div>
                    ) : null}

                    {checkForgery ? (
                      <div
                        style={{
                          width: '20px',
                          height: '20px',
                          background: '#E65101',
                          marginBottom: '20px',
                          borderRadius: '2px',
                          float: 'right',
                          marginRight: '20px'
                        }}
                        className='show_cause_box'
                      ></div>
                    ) : null}

                    {checkFlagged ? (
                      <div
                        style={{
                          width: '20px',
                          height: '20px',
                          background: '#DE3933',
                          marginBottom: '20px',
                          borderRadius: '2px',
                          float: 'right'
                        }}
                        className='show_cause_box'
                      ></div>
                    ) : null}
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <input
                    accept='image/*'
                    className={classes.input}
                    id='contained-button-file'
                    multiple
                    type='file'
                    style={{ display: 'none' }}
                    onChange={e => {
                      // setSelectedImage(e.target.files[0])

                      let img = new Image()
                      img.src = URL.createObjectURL(e.target.files[0])
                      //   img.onload = function() {
                      //     // alert(this.width + " " + this.height);
                      //     setImageDimensions({
                      //       height: img.height,
                      //       width: img.width
                      //     })
                      // };
                      img.onload = () => {
                        // alert(img.width + " " + img.height)
                        setImageDimensions({
                          height: img.height,
                          width: img.width
                        })
                        if (img.height > 300 || img.width > 300) {
                          e.target.value = null
                          setSelectedImage()
                          setIsOverSizeImage(true)
                        } else {
                          setSelectedImage(e.target.files[0])
                          setIsOverSizeImage(false)
                        }
                      }

                      // if(imageDimensions.height > 500){
                      //   e.target.value = null
                      // }

                      // setTimeout(() => {
                      //   e.target.value = null
                      // }, 2000)
                    }}
                  />
                  <label htmlFor='contained-button-file'>
                    {/* <Button
                      variant='contained'
                      color='primary'
                      component='span'
                    >
                      Upload
                    </Button> */}
                    <Button
                      variant='contained'
                      color='default'
                      component='span'
                      // className={classes.button}
                      startIcon={<AddPhotoAlternateIcon />}
                      style={{ background: '#4CAF50' }}
                    >
                      Add image
                    </Button>
                  </label>
                  {/* <InputLabel htmlFor='component-helper'>Add image</InputLabel>
                  <Input
                    fullWidth
                    id='component-helper'
                    aria-describedby='component-helper-text'
                    type='file'
                    onChange={e => {
                      setSelectedImage(e.target.files[0])
                      console.log(
                        'www-----',
                        URL.createObjectURL(e.target.files[0]),
                        e.target.files[0].name
                      )

                      let img = new Image()
                      img.src = URL.createObjectURL(e.target.files[0])
                      //   img.onload = function() {
                      //     // alert(this.width + " " + this.height);
                      //     setImageDimensions({
                      //       height: img.height,
                      //       width: img.width
                      //     })
                      // };
                      img.onload = () => {
                        // alert(img.width + " " + img.height)
                        setImageDimensions({
                          height: img.height,
                          width: img.width
                        })
                      }

                      // setTimeout(() => {
                      //   e.target.value = null
                      // }, 2000)
                    }}
                  /> */}
                </GridItem>
                <br />
                <br />
                <br />
                <br />
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    InputLabelProps={{ shrink: true }}
                    id='standard-basic'
                    label='Dealer Id'
                    variant='outlined'
                    disabled
                    value={generateDealerId}
                    fullWidth
                    // onChange={e => setZone(e.target.value)}
                  />{' '}
                  <br /> <br />
                </GridItem>

                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Dealer name'
                    variant='outlined'
                    value={name}
                    fullWidth
                    onChange={e => setName(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    name='dealerName'
                    error={errors['dealerName']}
                    {...(errors['dealerName'] && {
                      error: true,
                      helperText: errors['dealerName']
                    })}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />{' '}
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Shop name'
                    variant='outlined'
                    value={shopName}
                    fullWidth
                    onChange={e => setShopName(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    name='shopName'
                    error={errors['shopName']}
                    {...(errors['shopName'] && {
                      error: true,
                      helperText: errors['shopName']
                    })}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />{' '}
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Shop address'
                    variant='outlined'
                    value={shopAddress}
                    fullWidth
                    onChange={e => setShopAddress(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    name='shopAddress'
                    error={errors['shopAddress']}
                    {...(errors['shopAddress'] && {
                      error: true,
                      helperText: errors['shopAddress']
                    })}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />{' '}
                  <br /> <br />
                  {/* <TextareaAutosize fullWidth aria-label="minimum height" minRows={5} placeholder="Minimum 3 rows" /> */}
                  {/* <textarea rows='3' cols='42' name='comment' form='usrform'>
                    Enter text here...
                  </textarea> */}
                </GridItem>

                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    required
                    type='number'
                    max='11'
                    id='standard-basic'
                    label='Mobile number'
                    variant='outlined'
                    value={mobileNo}
                    fullWidth
                    onChange={e => setMobileNo(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    name='mobileNo'
                    error={errors['mobileNo']}
                    {...(errors['mobileNo'] && {
                      error: true,
                      helperText: errors['mobileNo']
                    })}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />{' '}
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    InputLabelProps={{ shrink: true }}
                    id='standard-basic'
                    label='Zone'
                    variant='outlined'
                    disabled
                    value={zone}
                    fullWidth
                    onChange={e => setZone(e.target.value)}
                  />{' '}
                  <br /> <br />
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      District
                    </InputLabel>
                    <Select
                      label='District'
                      fullWidth
                      value={`${district}`}
                      onChange={e => {
                        setDistrict(e.target.value)
                        districtCodeID()
                      }}
                      // onChange={handleInputValue}
                      // onBlur={handleInputValue}
                      // name='district'
                      error={errors['district']}
                      {...(errors['district'] && {
                        error: true,
                        helperText: errors['district']
                      })}
                      inputProps={{
                        name: 'district',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      {districtList.map(item => (
                        <MenuItem value={item.name} key={item.id}>
                          {item.name}
                        </MenuItem>
                      ))}

                      {/* <MenuItem value={'dhaka'}>Dhaka</MenuItem>
                    <MenuItem value={'rajshahi'}>Rajshahi</MenuItem>
                    <MenuItem value={'dinajpur'}>Dinajpur</MenuItem>
                    <MenuItem value={'mymensingh'}>Mymensingh</MenuItem>
                    <MenuItem value={'naogaon'}>Naogaon</MenuItem>
                    <MenuItem value={'faridpur'}>Faridpur</MenuItem> */}
                    </Select>
                  </FormControl>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Upazila
                    </InputLabel>
                    <Select
                      label='Upazila'
                      fullWidth
                      value={`${upazila}`}
                      onChange={e => {
                        setUpazila(e.target.value)
                        upazilaCodeID()
                      }}
                      // onChange={handleInputValue}
                      // onBlur={handleInputValue}
                      name='upazila'
                      error={errors['upazila']}
                      {...(errors['upazila'] && {
                        error: true,
                        helperText: errors['upazila']
                      })}
                      inputProps={{
                        name: 'upazila',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      <MenuItem value={''}>Select</MenuItem>
                      {upazilaList.map(item =>
                        item.district == district
                          ? item.upazilas.map(itemUpz => (
                              <MenuItem value={itemUpz.name} key={itemUpz.id}>
                                {itemUpz.name}
                              </MenuItem>
                            ))
                          : null
                      )}

                      {/* <MenuItem value={'adamdighi'}>Adamdighi</MenuItem>
                    <MenuItem value={'habiganj'}>Habiganj</MenuItem>
                    <MenuItem value={'bagha'}>Bagha</MenuItem>
                    <MenuItem value={'mymensingh'}>Mymensingh</MenuItem>
                    <MenuItem value={'mohadevpur'}>Mohadevpur</MenuItem>
                    <MenuItem value={'tanore'}>Tanore</MenuItem>
                    <MenuItem value={'badalgachi'}>Badalgachi</MenuItem> */}
                    </Select>
                  </FormControl>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Distance From Zonal office
                    </InputLabel>
                    <Select
                      label='Distance From Zonal office'
                      fullWidth
                      value={`${distanceZonal}`}
                      onChange={e => setDistanceZonal(e.target.value)}
                      // onChange={handleInputValue}
                      // onBlur={handleInputValue}
                      name='distanceZonal'
                      error={errors['distanceZonal']}
                      {...(errors['distanceZonal'] && {
                        error: true,
                        helperText: errors['distanceZonal']
                      })}
                      inputProps={{
                        name: 'distanceZonal',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      <MenuItem value={'0-50'}>0 - 50 km</MenuItem>
                      <MenuItem value={'51-100'}>51 - 100 km</MenuItem>
                      <MenuItem value={'100+'}>100+ km</MenuItem>
                    </Select>
                  </FormControl>
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <TextField
                    id='date'
                    label='Contract date'
                    fullWidth
                    type='date'
                    variant='outlined'
                    value={contractDate}
                    // defaultValue='2022-01-02'
                    onChange={e => setContractDate(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    name='contractDate'
                    error={errors['contractDate']}
                    {...(errors['contractDate'] && {
                      error: true,
                      helperText: errors['contractDate']
                    })}
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <TextField
                    id='date'
                    label='Contract renewal date'
                    fullWidth
                    type='date'
                    variant='outlined'
                    value={contractRenewalDate}
                    // defaultValue='2022-01-02'
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={e => setContractRenewalDate(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    // name='contractRenewalDate'
                    // error={errors['contractRenewalDate']}
                    // {...(errors['contractRenewalDate'] && {
                    //   error: true,
                    //   helperText: errors['contractRenewalDate']
                    // })}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <TextField
                    id='date'
                    label='Contract expiry date'
                    fullWidth
                    type='date'
                    variant='outlined'
                    value={contractExpiryDate}
                    // defaultValue='2022-01-02'
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={e => setContractExpiryDate(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    name='contractExpiryDate'
                    error={errors['contractExpiryDate']}
                    {...(errors['contractExpiryDate'] && {
                      error: true,
                      helperText: errors['contractExpiryDate']
                    })}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Contract Status
                    </InputLabel>
                    <Select
                      label='Contract Status'
                      fullWidth
                      value={`${contractStatus}`}
                      onChange={e => setContractStatus(e.target.value)}
                      // onChange={handleInputValue}
                      // onBlur={handleInputValue}
                      name='contractStatus'
                      error={errors['contractStatus']}
                      {...(errors['contractStatus'] && {
                        error: true,
                        helperText: errors['contractStatus']
                      })}
                      inputProps={{
                        name: 'contractStatus',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      <MenuItem value={'permanent'}>Permanent</MenuItem>
                      <MenuItem value={'new_dealer'}>New dealer</MenuItem>
                      {/* <MenuItem value={30}>Thirty</MenuItem> */}
                    </Select>
                  </FormControl>
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </GridItem>

        {/* start Family relation section */}
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader
              plain
              color='primary'
              style={{
                background: '#898b8a',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Family relation</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <br />
              <GridContainer></GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <TextField
                    id='dealerId'
                    label='Dealer ID'
                    fullWidth
                    type='number'
                    variant='outlined'
                    value={familyDealerId}
                    name='familyDealerId'
                    // defaultValue='2021-12-09'
                    className={classes.textField}
                    onChange={e => setFamilyDealerId(e.target.value)}
                    // onChange={handleInputValue}
                    onBlur={getDealerName}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </GridItem>

                <GridItem xs={12} sm={12} md={6}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Relation type
                    </InputLabel>
                    <Select
                      label='Relation type'
                      fullWidth
                      value={`${relationType}`}
                      name='relationType'
                      onChange={e => setRelationType(e.target.value)}
                      // {...(values.familyDealerId
                      //   ? {
                      //       value: values.relationType,
                      //       onChange: handleInputValue,
                      //       onBlur: handleInputValue,
                      //       error: errors['relationType'],
                      //       ...(errors['relationType'] && {
                      //         error: true,
                      //         helperText: errors['relationType']
                      //       })
                      //     }
                      //   : { disabled: true })}
                      inputProps={{
                        name: 'relationType',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      <MenuItem value={'father'}>Father</MenuItem>
                      <MenuItem value={'mother'}>Mother</MenuItem>
                      <MenuItem value={'son'}>Son</MenuItem>
                      <MenuItem value={'brother'}>Brother</MenuItem>
                      <MenuItem value={'daughter'}>Daughter</MenuItem>
                      <MenuItem value={'sister'}>Sister</MenuItem>
                      <MenuItem value={'uncle'}>Uncle</MenuItem>
                      <MenuItem value={'cousin'}>Cousin</MenuItem>
                    </Select>
                  </FormControl>
                </GridItem>

                <GridItem xs={12} sm={12} md={12} style={{ marginTop: '27px' }}>
                  <TextField
                    id='standard-basic'
                    label='Dealer Name'
                    variant='outlined'
                    value={familyDealerName}
                    fullWidth
                    disabled
                    // onChange={e => setZone(e.target.value)}
                    InputLabelProps={{ shrink: true }}
                  />{' '}
                  <br /> <br />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </GridItem>

        {/* start Dealer status section */}

        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader
              plain
              color='primary'
              style={{
                background: '#898b8a',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Dealer status</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={checkShowCause}
                          // onChange={handleChange}
                          onChange={e => setCheckShowCause(e.target.checked)}
                          name='showCause'
                          style={{ color: '#EA80FC' }}
                        />
                      }
                      label='SHOW CAUSE'
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={checkForgery}
                          // onChange={handleChange}
                          onChange={e => setCheckForgery(e.target.checked)}
                          name='checkForgery'
                          style={{ color: '#E65101' }}
                        />
                      }
                      label='FORGERY PROVED'
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={checkFlagged}
                          // onChange={handleChange}
                          onChange={e => setCheckFlagged(e.target.checked)}
                          name='checkFlagged'
                          style={{ color: '#DE3933' }}
                        />
                      }
                      label='FLAGGED'
                      // label={<SaveIcon />}
                    />
                  </FormGroup>
                </GridItem>
                <GridItem xs={12} sm={12} md={12} style={{ marginTop: '27px' }}>
                  <TextField
                    id='outlined-multiline-static'
                    label='Comment'
                    multiline
                    fullWidth
                    rows={4}
                    value={statusComment}
                    onChange={e => setStatusComment(e.target.value)}
                    variant='outlined'
                    InputLabelProps={{ shrink: true }}
                  />
                  <br />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </GridItem>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardBody>
              <br />
              <GridContainer>
                <div className={'assign_dealers'}>
                  <table>
                    <tr>
                      {/* <th>Select Dealer</th> */}
                      <th>Serial</th>
                      <th>Allotment number</th>
                      <th>Allotment date</th>
                      <th>Status</th>
                      <th>Payment</th>
                      <th>Warehouse</th>
                      {/* <th>Verify</th> */}
                    </tr>
                    {assignAllotmentList()}
                  </table>
                </div>
              </GridContainer>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </GridItem>

        <GridItem xs={12} sm={12} md={12} style={{ marginTop: '20px' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <Button
              type='submit'
              color='primary'
              // onClick={() => storeDealer()}
              onClick={handleOpen}
              style={{ background: '#4CAF50' }}
              // disabled={!formIsValid()}
            >
              Save dealer
            </Button>
            <Link
              href={`/admin/dealers?searchDealer=${query.searchDealer}&page=${query.page}`}
            >
              <Button
                color='primary'
                style={{ background: '#898B8A' }}
                // onClick={() => handleClose()}
              >
                Back
              </Button>
            </Link>
          </div>
        </GridItem>
      </GridContainer>
    </div>
  )
}

UserProfile.layout = Admin

export default UserProfile
