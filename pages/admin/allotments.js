import React from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import Table from 'components/Table/Table.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Button from 'components/CustomButtons/Button.js'
import Link from 'next/link'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import moment from 'moment'

import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import DeleteIcon from '@material-ui/icons/Delete'
import CancelIcon from '@material-ui/icons/Cancel'
import Pagination from '@material-ui/lab/Pagination'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import Router, { withRouter, useRouter } from 'next/router'
import VisibilityIcon from '@material-ui/icons/Visibility';
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import WithAuth from '../../components/WithAuth'

const useStylesModal = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

const styles = theme => ({
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0'
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF'
    }
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1'
    }
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    // backgroundColor: alpha(theme.palette.common.white, 0.15),
    // '&:hover': {
    //   backgroundColor: alpha(theme.palette.common.white, 0.25),
    // },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto'
    },
    border: '1px solid #D3D3D3'
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch'
    },
    marginTop: '8px'
  }
})

function AllotmentList () {
  const useStyles = makeStyles(styles)
  const classes = useStyles()
  const classesModal = useStylesModal()
  const [open, setOpen] = React.useState(false)
  const [checkDeletable, setCheckDeletable] = React.useState(false)
  const [deleteID, setDeleteID] = React.useState(99)

  const [resDatas, setResDatas] = useState([])
  const [refreshDeleteFlag, setRefreshDeleteFlag] = useState(true)
  const [clearSearchFlag, setClearSearchFlag] = useState(true)
  const [page, setPage] = useState(1)
  const [lastPage, setLastPage] = useState(1)
  const [allotmentSearchKey, setAllotmentSearchKey] = useState('')

  const { query } = useRouter()

  let buffer = []

  const handleOpen = id => {
    // setOpen(true)
    setDeleteID(id)
    isDeletable(id)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handlePaginaton = (event, value) => {
    value ? setPage(value) : setPage(1)

    let searchKeyTemp = ''
    searchKeyTemp = event.target.value

    let paginationData = axios({
      url: `${BASE_URL}/api/allotments?searchAllotment=${
        event.target.name === 'allotment_search_key'
          ? searchKeyTemp
          : allotmentSearchKey
      }&page=${value ? value : 1}&zone=${event.target.value}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        // console.log('Data print----- : ', response.data)

        setLastPage(response.data.last_page)

        response.data.data.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          buffer.push([
            // <Link href={`/admin/details-dealer?id=${item.id}`} className={stylesCustom.blackC}>
            //   {moment(item.allotment_date).format('MMMM Do YYYY')}
            // </Link>
            item.id,

            // <Link
            //   href={`/admin/details-allotment?id=${item.id}&searchAllotment=${
            //     event.target.name === 'allotment_search_key'
            //       ? searchKeyTemp
            //       : allotmentSearchKey
            //   }&page=${
            //     event.target.name === 'allotment_search_key' ? 1 : value
            //   }`}
            // >
            //   <a>{moment(item.allotment_date).format('MMMM Do YYYY')}</a>
            // </Link>,
            moment(item.allotment_date).format('MMMM Do YYYY'),
            item.fiscal_year,
            item.district,
            item.upazila,
            // `${item.distance_from_zonal} km`,
            // <Link href={`/admin/edit-dealer?id=${item.id}`}>
            //   <button class="MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss95 jss111 jss412" tabindex="0" type="button">
            //     <svg style={{ color: '#4caf50'}} class="MuiSvgIcon-root jss413" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"></path></svg>
            //   </button>
            // </Link>,
            // <button
            //   onClick={() => deleteAllotment(item.id)}
            //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
            //   tabindex='0'
            //   type='button'
            // >
            //   <svg
            //     style={{ color: '#f44336' }}
            //     class='MuiSvgIcon-root jss413'
            //     focusable='false'
            //     viewBox='0 0 24 24'
            //     aria-hidden='true'
            //   >
            //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
            //   </svg>
            // </button>
            <Link
              href={`/admin/details-allotment?id=${item.id}&searchAllotment=${
                event.target.name === 'allotment_search_key'
                  ? searchKeyTemp
                  : allotmentSearchKey
              }&page=${
                event.target.name === 'allotment_search_key' ? 1 : value
              }`}
            >
              <VisibilityIcon style={{ cursor: 'pointer' }} />
            </Link>,
            <button
              onClick={() => handleOpen(item.id)}
              class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
              tabindex='0'
              type='button'
            >
              <svg
                style={{ color: '#f44336' }}
                class='MuiSvgIcon-root jss413'
                focusable='false'
                viewBox='0 0 24 24'
                aria-hidden='true'
              >
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
              </svg>
            </button>
          ])
        )
        setResDatas(buffer)

        console.log(resDatas, '---buffer==')

        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }

  useEffect(() => {
    query.page && setPage(Number(query.page))
    query.searchAllotment && setAllotmentSearchKey(query.searchAllotment)

    let initialData = axios({
      url: `${BASE_URL}/api/allotments?searchAllotment=${
        query.searchAllotment ? query.searchAllotment : allotmentSearchKey
      }&page=${query.page ? query.page : page}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        // console.log('Data print----- : ', response.data)

        setLastPage(response.data.last_page)

        response.data.data.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          buffer.push([
            // <Link href={`/admin/details-dealer?id=${item.id}`} className={stylesCustom.blackC}>
            //   {moment(item.allotment_date).format('MMMM Do YYYY')}
            // </Link>
            item.id,

            // <Link
            //   href={`/admin/details-allotment?id=${item.id}&searchAllotment=${allotmentSearchKey}&page=${page}`}
            // >
            //   <a>{moment(item.allotment_date).format('MMMM Do YYYY')}</a>
            // </Link>,
            moment(item.allotment_date).format('MMMM Do YYYY'),
            item.fiscal_year,
            item.district,
            item.upazila,
            // `${item.distance_from_zonal} km`,
            // <Link href={`/admin/edit-dealer?id=${item.id}`}>
            //   <button class="MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss95 jss111 jss412" tabindex="0" type="button">
            //     <svg style={{ color: '#4caf50'}} class="MuiSvgIcon-root jss413" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"></path></svg>
            //   </button>
            // </Link>,
            // <button
            //   onClick={() => deleteAllotment(item.id)}
            //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
            //   tabindex='0'
            //   type='button'
            // >
            //   <svg
            //     style={{ color: '#f44336' }}
            //     class='MuiSvgIcon-root jss413'
            //     focusable='false'
            //     viewBox='0 0 24 24'
            //     aria-hidden='true'
            //   >
            //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
            //   </svg>
            // </button>
            <Link
              href={`/admin/details-allotment?id=${item.id}&searchAllotment=${allotmentSearchKey}&page=${page}`}
            >
              <VisibilityIcon style={{ cursor: 'pointer' }} />
            </Link>,
            <button
              onClick={() => handleOpen(item.id)}
              class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
              tabindex='0'
              type='button'
            >
              <svg
                style={{ color: '#f44336' }}
                class='MuiSvgIcon-root jss413'
                focusable='false'
                viewBox='0 0 24 24'
                aria-hidden='true'
              >
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
              </svg>
            </button>
          ])
        )
        setResDatas(buffer)

        console.log(resDatas, '---buffer==')

        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }, [refreshDeleteFlag, clearSearchFlag])

  const clearSearchFilter = () => {
    query.page && setPage(Number(query.page))

    let initialData = axios({
      url: `${BASE_URL}/api/allotments?searchAllotment=&page=${
        query.page ? query.page : page
      }`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        // console.log('Data print----- : ', response.data)

        setLastPage(response.data.last_page)

        response.data.data.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          buffer.push([
            // <Link href={`/admin/details-dealer?id=${item.id}`} className={stylesCustom.blackC}>
            //   {moment(item.allotment_date).format('MMMM Do YYYY')}
            // </Link>
            item.id,
            
            // <Link
            //   href={`/admin/details-allotment?id=${item.id}&searchAllotment=${allotmentSearchKey}&page=${page}`}
            // >
            //   <a>{moment(item.allotment_date).format('MMMM Do YYYY')}</a>
            // </Link>,
            moment(item.allotment_date).format('MMMM Do YYYY'),
            item.fiscal_year,
            item.district,
            item.upazila,
            // `${item.distance_from_zonal} km`,
            // <Link href={`/admin/edit-dealer?id=${item.id}`}>
            //   <button class="MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss95 jss111 jss412" tabindex="0" type="button">
            //     <svg style={{ color: '#4caf50'}} class="MuiSvgIcon-root jss413" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"></path></svg>
            //   </button>
            // </Link>,
            // <button
            //   onClick={() => deleteAllotment(item.id)}
            //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
            //   tabindex='0'
            //   type='button'
            // >
            //   <svg
            //     style={{ color: '#f44336' }}
            //     class='MuiSvgIcon-root jss413'
            //     focusable='false'
            //     viewBox='0 0 24 24'
            //     aria-hidden='true'
            //   >
            //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
            //   </svg>
            // </button>
            <Link
              href={`/admin/details-allotment?id=${item.id}&searchAllotment=${allotmentSearchKey}&page=${page}`}
            >
              <VisibilityIcon style={{ cursor: 'pointer' }} />
            </Link>,
            <button
              onClick={() => handleOpen(item.id)}
              class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
              tabindex='0'
              type='button'
            >
              <svg
                style={{ color: '#f44336' }}
                class='MuiSvgIcon-root jss413'
                focusable='false'
                viewBox='0 0 24 24'
                aria-hidden='true'
              >
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
              </svg>
            </button>
          ])
        )
        setResDatas(buffer)

        console.log(resDatas, '---buffer==')

        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }

  const deleteAllotment = id => {
    return (
      axios({
        url: `${BASE_URL}/api/allotments/delete/${id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'DELETE'
      })
        .then(response => {
          console.log('delete allotment print----- : ', response.data)
          setRefreshDeleteFlag(!refreshDeleteFlag)
          handleClose()
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('delete error print----- : ', err)
        })
    )
  }
  const isDeletable = id => {
    return (
      axios({
        url: `${BASE_URL}/api/allotments/isDeletable/${id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'get'
      })
        .then(response => {
          console.log('isDeletable allotment print----- : ', response.data)
          
          if(response.data.success){
            console.log('true-----')
            setCheckDeletable(true)
            setOpen(true)
          } else{
            console.log('false-----')
            setCheckDeletable(false)
            setOpen(true)
          }

        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  return (
    <Admin>
      <GridContainer>
        <Modal
          aria-labelledby='transition-modal-title'
          aria-describedby='transition-modal-description'
          className={classesModal.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={open}>
            <div className={classesModal.paper}>
              {checkDeletable ? (<>
              <h2 id='transition-modal-title'>Delete?</h2>
              <p id='transition-modal-description'>
                You can't undo this action.
              </p>
              <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                startIcon={<CancelIcon />}
                onClick={() => handleClose()}
              >
                Cancel
              </Button>
              <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                style={{ background: '#ff3a3a', marginLeft: '20px' }}
                startIcon={<DeleteIcon />}
                onClick={() => deleteAllotment(deleteID)}
              >
                Delete
              </Button>
              </>) : (<>
              <h2 id='transition-modal-title'>Sorry!</h2>
              <p id='transition-modal-description' style={{ marginRight: '50px' }}>
                You can't delete this allotment.
              </p>
              <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                // startIcon={<CancelIcon />}
                onClick={() => handleClose()}
              >
                Close
              </Button>
              </>)}
            </div>
          </Fade>
        </Modal>

        <div className={classes.search} style={{ height: '56px' }}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder='Enter district/upazila'
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput
            }}
            value={allotmentSearchKey}
            inputProps={{ 'aria-label': 'search' }}
            name='allotment_search_key'
            onChange={e => {
              setAllotmentSearchKey(e.target.value)
              e.target.value.length > 2
                ? handlePaginaton(e)
                : clearSearchFilter()
            }}
            style={{ marginTop: '2px' }}
          />
        </div>

        {Cookies.get('cRole') === 'superadmin' && (
          <div style={{ width: '200px' }}>
          <FormControl
            fullWidth
            variant='outlined'
            className={classes.formControl}
          >
            <InputLabel
              style={{ marginBottom: '10px' }}
              htmlFor='outlined-age-simple'
            >
              Select zone
            </InputLabel>
            <Select
              label='Contract Status'
              fullWidth
              // value={{
              //   age: '',
              //   name: 'hai',
              //   labelWidth: 0,
              // }}
              // value={values.contractStatus}
              // onChange={e => setContractStatus(e.target.value)}
              onChange={handlePaginaton}
              // onBlur={handleInputValue}
              name='contractStatus'
              // error={errors['contractStatus']}
              // {...(errors['contractStatus'] && {
              //   error: true,
              //   helperText: errors['contractStatus']
              // })}
              // input={
              //   <OutlinedInput
              //     // labelWidth={100}
              //     name='age'
              //     id='outlined-age-simple'
              //   />
              // }
              inputProps={{
                name: 'contractStatus',
                id: 'outlined-age-native-simple'
              }}
            >
              {/* <MenuItem value="">
                      <em>None</em>
                    </MenuItem> */}
              <MenuItem value={'Dhaka'}>Dhaka</MenuItem>
              <MenuItem value={'Rajshahi'}>Rajshahi</MenuItem>
              <MenuItem value={'Chattogram'}>Chattogram</MenuItem>
              <MenuItem value={'Khulna'}>Khulna</MenuItem>
              <MenuItem value={'Barishal'}>Barishal</MenuItem>
              <MenuItem value={'Moulvibazar'}>Moulvibazar</MenuItem>
              <MenuItem value={'Rangpur'}>Rangpur</MenuItem>
              <MenuItem value={'Mymensingh'}>Mymensingh (Camp)</MenuItem>
              {/* <MenuItem value={30}>Thirty</MenuItem> */}
            </Select>
          </FormControl>
        </div>
        )}
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color='primary' style={{ background: '#4CAF50' }}>
              <h4 className={classes.cardTitleWhite}>Allotments</h4>
              <p className={classes.cardCategoryWhite}>
                {/* Here is a subtitle for this table */}
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor='primary'
                tableHead={[
                  'Allotment no',
                  'Allotment date',
                  'Fiscal year',
                  'District',
                  'Upazila',
                  'Actions',
                  ''
                ]}
                // tableData={[
                //   ["Dec 10, 2021 ", "2020 - 2021", "Rajshahi", "Tanore", "0-50 km"],
                //   ["Nov 26, 2021 ", "2020 - 2021", "Dinajpur", "Habiganj", "50-100 km"],
                //   ["Nov 26, 2021 ", "2020 - 2021", "Rajshahi", "Bagha", "100-150 km"],
                //   ["Nov 12, 2021 ", "2020 - 2021", "Dinajpur", "Habiganj", "0-50 km"],
                //   ["Oct 09, 2021 ", "2020 - 2021", "Mymensingh", "Mymensingh", "0-50 km"],
                //   ["Sep 20, 2021 ", "2020 - 2021", "Naogaon", "Badalgachi", "50-100 km"],
                //   ["Sep 16, 2021 ", "2020 - 2021", "Dinajpur", "Habiganj", "0-50 km"],

                // ]}
                tableData={resDatas}
              />
              <br />
              <br />
              <Pagination
                count={lastPage}
                page={page}
                onChange={handlePaginaton}
              />
            </CardBody>
            <CardFooter>
              <Link
                href={`/admin/add-new-allotment?searchAllotment=${allotmentSearchKey}&page=${page}`}
              >
                <Button color='primary' style={{ background: '#4CAF50' }}>
                  Add Allotment
                </Button>
              </Link>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </Admin>
  )
}

// AllotmentList.layout = Admin

export default WithAuth(AllotmentList, ['superadmin', 'admin'])
