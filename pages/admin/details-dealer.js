import React, {useState, useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// layout for this page
import Admin from "layouts/Admin.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import avatar from "assets/img/faces/avatar-159236_1280.png";
// import avatar from "assets/img/faces/profile_photo.png";

import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import MenuItem from '@material-ui/core/MenuItem';
import axios from 'axios';
import Router, { withRouter } from 'next/router'
import { useRouter } from "next/router";
import Table from "components/Table/Table.js";
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'



const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
  },
};

function DetailsDealer() {
  const useStyles = makeStyles(styles);
  const classes = useStyles();

  const [name, setName] = useState();
  const [shopName, setShopName] = useState();
  const [mobileNo, setMobileNo] = useState();
  const [zone, setZone] = useState();
  const [district, setDistrict] = useState();
  const [upazila, setUpazila] = useState();
  const [distanceZonal, setDistanceZonal] = useState();
  const [shopAddress, setShopAddress] = useState();
  const [contractStatus, setContractStatus] = useState();

  const [dealerSingle, setDealerSingle] = useState({ 
    name: '', 
    mobile_no: '',
    district: '',
    distance_from_zonal: '',
    contract_status: '',
    shop_address: '',
    shop_name: '',
    upazila: '',
    zone: ''
  })

  // const [shopName, setShopName] = useState();
  // const [shopName, setShopName] = useState();

  const { query } = useRouter();
  let susam = {};

  useEffect(() => {
    let dealerData = axios({
      url: `${BASE_URL}/api/dealers/${query.id}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Cookies.get('cToken')}`,
      },
      // data: submitData,
      method: 'get',
    })
      .then((response) => {
        console.log('Single from use effect print----- : ', response.data)
      
        console.log(dealerSingle);
        let data = response.data;
        console.log(susam.name,'----susam---');

        setDealerSingle({ 
          name: response.data.name, 
          mobile_no: response.data.mobile_no,
          district: response.data.district,
          distance_from_zonal: response.data.distance_from_zonal,
          contract_status: response.data.contract_status,
          shop_address: response.data.shop_address,
          shop_name: response.data.shop_name,
          upazila: response.data.upazila,
          zone: response.data.zone
        });

        setName(data.name);
        setShopName(data.shop_name);
        setMobileNo(data.mobile_no);
        setZone(data.zone);
        setDistrict(data.district);
        setUpazila(data.upazila);
        setDistanceZonal(data.distance_from_zonal);
        setShopAddress(data.shop_address);
        setContractStatus(data.contract_status);

        return response.data;
        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch((err) => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })

  }, [])

  const updateDealer = () => {

    const submitData = {
      name: name,
      shop_name : shopName,
      distance_from_zonal: distanceZonal,
      shop_address: shopAddress,
      mobile_no: mobileNo,
      zone: zone,
      district: district,
      upazila: upazila,
      contract_status: "Permanent",
    }

    return axios({
      url: `${BASE_URL}/api/update/${query.id}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Cookies.get('cToken')}`,
      },
      data: submitData,
      method: 'put',
    })
      .then((response) => {
        console.log('token print----- : ', response.data)
        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch((err) => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })

  }

  return (
    <div>
      <GridContainer>

        <GridItem xs={12} sm={12} md={12}>
          <Card profile>
          <CardHeader color="primary" style={{ background: '#4CAF50', marginBottom: '100px', display: 'flex'}}>
              <h4 className={classes.cardTitleWhite}>Dealer Details</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardAvatar profile>
              <a href="#pablo" onClick={(e) => e.preventDefault()}>
                <img src={avatar} alt="..." />
              </a>
            </CardAvatar>
            <CardBody profile>
              
<br />

              <Table
              tableHeaderColor="primary"
              // tableHead={["Dealer name", "Mobile number"]}
              tableData={[
                ["Dealer name : ", name],
                ["Shop name : ", shopName],
                ["Mobile number : ", mobileNo],
                ["Zone : ", zone],
                ["District : ", district],
                ["Upazila : ", upazila],
                ["Distance from zonal office : ", distanceZonal],
                ["Shop address : ", shopAddress],
                ["Contract Status : ", contractStatus],
              //   ["Minerva Hooper", "01824284954", "Syed Traders", "Dhaka", "0-50 km", "Permanent"],
              //   ["Minerva Hooper", "01824284954", "Syed Traders", "Dhaka", "0-50 km", "Permanent"],
              //   ["Minerva Hooper", "01824284954", "Syed Traders", "Dhaka", "0-50 km", "New Dealer"],
              //   ["Minerva Hooper", "01824284954", "Syed Traders", "Dhaka", "0-50 km", "Permanent"],
              //   ["Minerva Hooper", "01824284954", "Syed Traders", "Dhaka", "0-50 km", "Permanent"],
              //   ["Minerva Hooper", "01824284954", "Syed Traders", "Dhaka", "0-50 km", "New Dealer"],
              //   ["Minerva Hooper", "01824284954", "Syed Traders", "Dhaka", "0-50 km", "Permanent"],
                
                

              ]}
            />


            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}

DetailsDealer.layout = Admin;

export default DetailsDealer;
