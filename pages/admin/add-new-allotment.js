import React from 'react'
// @material-ui/core components
import { makeStyles, useTheme } from '@material-ui/core/styles'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import Table from 'components/Table/Table.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Button from 'components/CustomButtons/Button.js'
import Link from 'next/link'
import axios from 'axios'
import { useEffect, useState } from 'react'
import Router, { withRouter, useRouter } from 'next/router'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import { districtList, upazilaList } from './constData.json'
import stylesCustom from './add-new-allotment.module.css'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import Chip from '@material-ui/core/Chip'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import SaveIcon from '@material-ui/icons/Save'
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

import Checkbox from '@material-ui/core/Checkbox'
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'
import { useFormControls } from '../../components/allotments/allotmentValidation'
import FormControl from '@material-ui/core/FormControl'
import ListItemText from '@material-ui/core/ListItemText'
import Pagination from '@material-ui/lab/Pagination'

// const styles = {
//   cardCategoryWhite: {
//     '&,& a,& a:hover,& a:focus': {
//       color: 'rgba(255,255,255,.62)',
//       margin: '0',
//       fontSize: '14px',
//       marginTop: '0',
//       marginBottom: '0'
//     },
//     '& a,& a:hover,& a:focus': {
//       color: '#FFFFFF'
//     }
//   },
//   cardTitleWhite: {
//     color: '#FFFFFF',
//     marginTop: '0px',
//     minHeight: 'auto',
//     fontWeight: '300',
//     fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
//     marginBottom: '3px',
//     textDecoration: 'none',
//     '& small': {
//       color: '#777',
//       fontSize: '65%',
//       fontWeight: '400',
//       lineHeight: '1'
//     }
//   },
//   search: {
//     position: 'relative',
//     borderRadius: theme.shape.borderRadius,
//     backgroundColor: alpha(theme.palette.common.white, 0.15),
//     '&:hover': {
//       backgroundColor: alpha(theme.palette.common.white, 0.25),
//     },
//     marginRight: theme.spacing(2),
//     marginLeft: 0,
//     width: '100%',
//     [theme.breakpoints.up('sm')]: {
//       marginLeft: theme.spacing(3),
//       width: 'auto',
//     },
//   },
//   searchIcon: {
//     padding: theme.spacing(0, 2),
//     height: '100%',
//     position: 'absolute',
//     pointerEvents: 'none',
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   inputRoot: {
//     color: 'inherit',
//   },
//   inputInput: {
//     padding: theme.spacing(1, 1, 1, 0),
//     // vertical padding + font size from searchIcon
//     paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
//     transition: theme.transitions.create('width'),
//     width: '100%',
//     [theme.breakpoints.up('md')]: {
//       width: '20ch',
//     },
//   },
// }

const useStylesModal = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: '85vw',
    height: '90vh',
    overflow: 'scroll'
  },
  paperSave: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

function AddNewAllotment () {
  const classesModal = useStylesModal()
  const [open, setOpen] = React.useState(false)
  const [openSaveModal, setOpenSaveModal] = React.useState(false)
  const [openProductRemoveModal, setOpenProductRemoveModal] = React.useState(
    false
  )
  const [page, setPage] = useState(1)
  const [lastPage, setLastPage] = useState(1)

  const [deleteIndex, setDeleteIndex] = React.useState(99)
  const { query } = useRouter()

  const {
    handleInputValue,
    handleFormSubmit,
    formIsValid,
    errors,
    values
  } = useFormControls()

  const handleOpen = id => {
    setOpen(true)
  }
  const handleClose = () => {
    setSelectedDealerIds([])
    setOpen(false)
  }

  const handleOpenSaveModal = () => {
    let dealerMobileNumbers = []
    dealerList.map(iteam => {
      if (mainSelectedDealerIds.includes(iteam.dealer_id)) {
        dealerMobileNumbers.push(iteam.mobile_no)
      }
    })
    setSelectedDealersNo(dealerMobileNumbers)
    setOpenSaveModal(true)
  }

  const handleCloseSaveModal = () => {
    setOpenSaveModal(false)
  }

  const handleOpenProductRemoveModal = index => {
    setOpenProductRemoveModal(true)
    setDeleteIndex(index)
  }

  const handleCloseProductRemoveModal = () => {
    setOpenProductRemoveModal(false)
  }

  const useStyles = makeStyles(theme => ({
    cardCategoryWhite: {
      '&,& a,& a:hover,& a:focus': {
        color: 'rgba(255,255,255,.62)',
        margin: '0',
        fontSize: '14px',
        marginTop: '0',
        marginBottom: '0'
      },
      '& a,& a:hover,& a:focus': {
        color: '#FFFFFF'
      }
    },
    cardTitleWhite: {
      color: '#FFFFFF',
      marginTop: '0px',
      minHeight: 'auto',
      fontWeight: '300',
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: '3px',
      textDecoration: 'none',
      '& small': {
        color: '#777',
        fontSize: '65%',
        fontWeight: '400',
        lineHeight: '1'
      }
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      // backgroundColor: alpha(theme.palette.common.white, 0.15),
      // '&:hover': {
      //   backgroundColor: alpha(theme.palette.common.white, 0.25),
      // },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto'
      },
      border: '1px solid #D3D3D3'
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    inputRoot: {
      color: 'inherit'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch'
      },
      marginTop: '8px'
    }
  }))
  const classes = useStyles()

  const [resDatas, setResDatas] = useState([])
  const [dealerList, setDealerList] = useState([])
  const [recentAllotedDealerList, setRecentAllotedDealerList] = useState([])
  const [excludeDateDealerList, setExcludeDateDealerList] = useState([])
  const [excludeDate, setExcludeDate] = useState('')
  const [allotmentId, setAllotmentId] = useState()
  const [contractExpiryList, setContractExpiryList] = useState([])
  const [dealerIdList, setDealerIdList] = useState(['013701011', '013701015'])
  const [refreshDeleteFlag, setRefreshDeleteFlag] = useState(true)
  const [clearSearchFlag, setClearSearchFlag] = useState(true)
  const [zone, setZone] = useState('Dhaka')
  const [ward, setWard] = useState()
  const [district, setDistrict] = useState()
  const [upazila, setUpazila] = useState()
  const [distanceZonal, setDistanceZonal] = useState()
  const [allotmentDate, setAllotmentDate] = useState()
  const [fromFilter, setFromFilter] = useState('')
  const [toFilter, setToFilter] = useState('')
  const [searchKey, setSearchKey] = useState('')
  const [clearSelected, setClearSelected] = useState(['1', '2'])

  const [todos, setTodos] = useState(['013701011', '013701015'])

  const [subTotal, setSubTotal] = useState(0)
  const [totalPrice, setTotalPrice] = useState(0)

  const [inputList, setInputList] = useState([
    {
      product_name: '',
      amount: '',
      unit: '',
      price: '',
      transport_cost: '',
      total_price: ''
    }
  ])

  const [selectedDate, setSelectedDate] = React.useState(
    new Date('2014-08-18T21:11:54')
  )

  // for checkbox----

  const [checked, setChecked] = React.useState(false)
  const [checkedRecentAlloted, setCheckedRecentAlloted] = useState(false)
  const [checkedContractExpiry, setCheckedContractExpiry] = useState(false)
  const [checkedFamilyMember, setCheckedFamilyMember] = useState(false)
  const [selectedDealerIds, setSelectedDealerIds] = useState([])
  const [mainSelectedDealerIds, setMainSelectedDealerIds] = useState([])
  const [selectedDealersNo, setSelectedDealersNo] = useState([])
  const [removeDealerIds, setRemoveDealerIds] = useState([])
  const [personName, setPersonName] = React.useState([])
  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
    checkedC: false,
    checkedG: false
  })

  const names = ['recentAllotedDealers', 'contractExpiry', 'sameFamilyMember']
  const ITEM_HEIGHT = 48
  const ITEM_PADDING_TOP = 8
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250
      }
    }
  }
  // const handleChange = event => {
  //   setChecked(event.target.checked)
  // }

  const handleAdd = () => {
    const newTodos = [...todos]
    newTodos.push('ashraf')
    setTodos(newTodos)
    console.log('newTodos--', todos)
  }

  const handleDontInclude = event => {
    console.log('event handleDontInclude---', event.target)

    setPersonName(event.target.value)

    // event.target.value.map(iteam => {

    event.target.value.includes('recentAllotedDealers')
      ? setCheckedRecentAlloted(true)
      : setCheckedRecentAlloted(false)

    event.target.value.includes('contractExpiry')
      ? setCheckedContractExpiry(true)
      : setCheckedContractExpiry(false)

    event.target.value.includes('sameFamilyMember')
      ? setCheckedFamilyMember(true)
      : setCheckedFamilyMember(false)

    // })

    // if (event.target.name === 'recentAllotedDealers') {
    //   setCheckedRecentAlloted(event.target.checked)
    // }
    // if (event.target.name === 'contractExpiry') {
    //   setCheckedContractExpiry(event.target.checked)
    // }
    // if (event.target.name === 'sameFamilyMember') {
    //   setCheckedFamilyMember(event.target.checked)
    // }
  }

  const handleChange = event => {
    // const selectedId = parseInt(event.target.name);
    const selectedId = event.target.name
    console.log('event-----', event.target.name)

    // let newIds = [...dealerIdList]
    // console.log('newIds--', dealerIdList)
    // console.log('newIds--', newIds)
    // newIds.push(event.target.name)
    // console.log('newIds--2', newIds)
    // setDealerIdList(newIds)

    // Check if "ids" contains "selectedIds"
    // If true, this checkbox is already checked
    // Otherwise, it is not selected yet

    if (selectedDealerIds.includes(selectedId)) {
      console.log('ids include true')
      const newIds = selectedDealerIds.filter(id => id !== selectedId)
      setSelectedDealerIds(newIds)
      console.log('newIds--2', selectedDealerIds)
    } else {
      console.log('ids include False')
      const newIds = [...selectedDealerIds]
      console.log('newIds--', newIds)
      newIds.push(selectedId)
      console.log('newIds--2', newIds)
      setSelectedDealerIds(newIds)
    }
  }

  const handleAssignDealers = event => {
    // const mainIds = [...selectedDealerIds]
    const mainIds = [...mainSelectedDealerIds, ...selectedDealerIds]
    setMainSelectedDealerIds(mainIds)
  }

  const handleRemoveChange = event => {
    const removedId = event.target.name

    if (removeDealerIds.includes(removedId)) {
      const newIds = removeDealerIds.filter(id => id !== removedId)
      setRemoveDealerIds(newIds)
    } else {
      const newIds = [...removeDealerIds]
      newIds.push(removedId)
      setRemoveDealerIds(newIds)
    }
  }
  const handleRemoveDealers = () => {
    let mainIds = [...mainSelectedDealerIds]
    removeDealerIds.map(item => (mainIds = mainIds.filter(id => id !== item)))
    setMainSelectedDealerIds(mainIds)
    setRemoveDealerIds([])
  }
  const handleCancelRemove = () => {
    setRemoveDealerIds([])
  }

  const sendBulkSms = () => {
    return (
      axios({
        url: `${BASE_URL}/api/send-bulk-sms`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: {
          selectedDealersNo: selectedDealersNo,
          allotment_date: values.allotmentDate
        },
        method: 'post'
      })
        .then(response => {
          console.log('sms resp----', response)
          // return response
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  // end for checkbox----

  const handleDateChange = date => {
    setSelectedDate(date)
  }

  const showTotal = total => {
    setTotalPrice(total)
    return total
  }

  let buffer = []
  let bufferDealer = []

  useEffect(() => {
    let calcSub = 0

    inputList.map((x, i) => {
      if (x.amount && x.price && x.transport_cost) {
        calcSub = calcSub + +(x.amount * x.price) + +x.transport_cost
        setSubTotal(calcSub)
      }
    })

    // {x.amount && x.price && x.transport_cost
    //   ? showSubTotal(+(x.amount * x.price) + +x.transport_cost)
    //   : ''}
  }, [inputList])

  const assignDealersNoFilter = (event, value) => {
    value ? setPage(value) : setPage(1)
    let bufferContractExpire = []
    let today = new Date()
    return (
      axios({
        url: `${BASE_URL}/api/dealers?upazila=${values.upazila}&page=${
          value ? value : 1
        }&ward=${ward ? ward : ''}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'get'
      })
        .then(response => {
          // setResDatas(response.data);
          console.log('assignDealersNoFilter Data print----- : ', response.data)

          setLastPage(response.data.last_page)

          response.data.data.map(item => {
            // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

            bufferDealer.push([
              item.dealer_id,
              item.name,
              item.mobile_no,
              item.shop_name,
              item.upazila,
              item.contract_status,
              item.contract_expiry_date,
              item.family_dealer_id
            ])
            const Difference_In_Days =
              (new Date(item.contract_expiry_date).getTime() -
                today.getTime()) /
              (1000 * 3600 * 24)

            console.log('Difference_In_Days----2:', Difference_In_Days)

            if (Difference_In_Days < 30) {
              bufferContractExpire.push(item.dealer_id)
            }
          })

          // setDealerList(bufferDealer)
          setDealerList(response.data.data)
          setContractExpiryList(bufferContractExpire)

          console.log('---Dealer buffer--- :', dealerList)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('dealer token print----- : ', err)
        })
    )
  }

  useEffect(() => {
    axios({
      url: `${BASE_URL}/api/allotments/last-allotment-id`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      method: 'get'
    })
      .then(response => {
        console.log('last Allotment responseData----', response.data)
        // if (response.data.success) {
        //   let dealerIdData = response.data.data + 1

        //   console.log(
        //     'last dealerID print-----ff : ',
        //     dealerIdData.toString().charAt(0)
        //   )

        //   if (dealerIdData.toString().charAt(0) == 0) {
        //     setGenerateDealerId(`${dealerIdData}`)
        //   } else {
        //     setGenerateDealerId(`0${dealerIdData}`)
        //   }
        // } else {
        //   console.log('success false---', response.data.success)

        //   setGenerateDealerId(`${partialId}001`)
        // }
        setAllotmentId(response.data.data.id)
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }, [])

  useEffect(() => {
    let bufferDontInclude = []
    // let bufferContractExpire = []
    let today = new Date()
    axios({
      url: `${BASE_URL}/api/allotments/recently-alloted-dealers`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('recently-alloted Dealer Data print----- : ', response.data)

        response.data.map(item => {
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          bufferDontInclude.push(item.dealer_id)

          // const Difference_In_Days =
          //   (new Date(item.product.contract_expiry_date).getTime() -
          //     today.getTime()) /
          //   (1000 * 3600 * 24)

          // console.log('Difference_In_Days----:', Difference_In_Days)

          // if (Difference_In_Days < 30) {
          //   bufferContractExpire.push(item.product_id)
          // }
        })

        setRecentAllotedDealerList(bufferDontInclude)

        // setContractExpiryList(bufferContractExpire)

        console.log('---Recent Alloted buffer--- :', recentAllotedDealerList)
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('Recent Alloted print----- : ', err)
      })
  }, [])

  useEffect(() => {
    let bufferContractExpire = []
    let initialData = axios({
      url: `${BASE_URL}/api/dealers?upazila=${values.upazila}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('Dealer Data print----- : ', response.data)

        response.data.map(item => {
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          bufferDealer.push([
            item.dealer_id,
            item.name,
            item.mobile_no,
            item.shop_name,
            item.upazila,
            item.contract_status,
            item.contract_expiry_date,
            item.family_dealer_id
          ])

          const Difference_In_Days =
            (new Date(item.contract_expiry_date).getTime() - today.getTime()) /
            (1000 * 3600 * 24)

          console.log('Difference_In_Days----:', Difference_In_Days)

          if (Difference_In_Days < 30) {
            bufferContractExpire.push(item.dealer_id)
          }
        })

        // setDealerList(bufferDealer)
        setDealerList(response.data)
        setContractExpiryList(bufferContractExpire)

        console.log('---Dealer buffer--- :', dealerList)
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('dealer token print----- : ', err)
      })
  }, [values.upazila])

  useEffect(() => {
    inputList.map((x, i) =>
      // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

      buffer.push([
        <Select
          fullWidth
          // value={{
          //   age: '',
          //   name: 'hai',
          //   labelWidth: 0,
          // }}
          value={x.product_name}
          onChange={e => handleInputChange(e, i)}
          input={
            <OutlinedInput
              // labelWidth={100}
              name='product_name'
              id='outlined-age-simple'
            />
          }
        >
          <MenuItem value={'sugar'}>Sugar</MenuItem>
          <MenuItem value={'rice'}>Rice</MenuItem>
          <MenuItem value={'onion'}>Onion</MenuItem>
          <MenuItem value={'soyabean2'}>soyabean oil - 2 litre</MenuItem>
          <MenuItem value={'soyabean5'}>soyabean oil - 5 litre</MenuItem>
          <MenuItem value={'Potato'}>Potato</MenuItem>
        </Select>,
        <div style={{ display: 'flex', maxWidth: '250px' }}>
          <TextField
            id='standard-basic'
            label=''
            type='number'
            name='amount'
            value={x.amount}
            variant='outlined'
            onChange={e => handleInputChange(e, i)}
            style={{ marginRight: '10px' }}
          />

          <Select
            fullWidth
            // value={{
            //   age: '',
            //   name: 'hai',
            //   labelWidth: 0,
            // }}
            value={x.unit}
            onChange={e => handleInputChange(e, i)}
            input={
              <OutlinedInput
                // labelWidth={100}
                name='unit'
                id='outlined-age-simple'
              />
            }
          >
            <MenuItem value={'kg'}>Kg</MenuItem>
            <MenuItem value={'litre'}>Litre</MenuItem>
          </Select>
        </div>,
        <TextField
          id='standard-basic'
          label=''
          name='price'
          value={x.price}
          variant='outlined'
          onChange={e => handleInputChange(e, i)}
        />,
        <TextField
          id='standard-basic'
          label=''
          name='transport_cost'
          value={x.transport_cost}
          variant='outlined'
          onChange={e => handleInputChange(e, i)}
        />,
        <>
          {x.amount && x.price && x.transport_cost
            ? showTotal(+(x.amount * x.price) + +x.transport_cost)
            : ''}
          {}
        </>,
        <button
          // onClick={() => handleOpen(item.id)}
          // onClick={() => handleRemoveClick(i)}
          onClick={() => handleOpenProductRemoveModal(i)}
          class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
          tabindex='0'
          type='button'
        >
          <svg
            style={{ color: '#f44336' }}
            class='MuiSvgIcon-root jss413'
            focusable='false'
            viewBox='0 0 24 24'
            aria-hidden='true'
          >
            <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
          </svg>
        </button>
      ])
    )

    // buffer.push([
    //   "", "", "", "Subtotal : ", subTotal
    // ])
    setResDatas(buffer)
  }, [inputList])

  const deleteDealer = id => {
    return (
      axios({
        url: `${BASE_URL}/api/delete/${id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'DELETE'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          // return Router.push('/admin/table-list')
          setRefreshDeleteFlag(!refreshDeleteFlag)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target
    const list = [...inputList]
    list[index][name] = value
    if (list[index].amount && list[index].price && list[index].transport_cost) {
      list[index].total_price =
        +(list[index].amount * list[index].price) + +list[index].transport_cost
    }
    setInputList(list)
  }

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...inputList]
    list.splice(index, 1)
    setInputList(list)
    handleCloseProductRemoveModal()
  }

  // handle click event of the Add button
  const handleAddClick = () => {
    setInputList([
      ...inputList,
      {
        product_name: '',
        amount: '',
        unit: '',
        price: '',
        transport_cost: '',
        total_price: ''
      }
    ])
  }
  const handleSearch = e => {
    console.log('e------:', e)
    let searchKeyTemp = ''
    let fromFilterTemp = ''
    let toFilterTemp = ''

    if (e.target.name === 'search_key') {
      searchKeyTemp = e.target.value
      // setSearchKey(e.target.value)
    }
    if (e.target.name === 'searchTo') {
      if (fromFilter && e.target.value) {
        fromFilterTemp = fromFilter - 1
        toFilterTemp = e.target.value - fromFilter + 1
      }
    }

    let searchUrl = `${BASE_URL}/api/search-dealer?searchKey=${
      e.target.name === 'search_key' ? searchKeyTemp : searchKey
    }&from=${fromFilterTemp}&to=${toFilterTemp}&upazila=${values.upazila}`
    // if (searchKey) {
    //   searchUrl = `${BASE_URL}/api/search-dealer/${searchKey}`
    // } else searchUrl = `${BASE_URL}/api/dealers`

    // if (fromFilter && toFilter) {
    //   searchUrl = `${BASE_URL}/api/search-dealer?from=${fromFilter}&to=${toFilter}`
    //   // searchUrl = `${BASE_URL}/api/search-dealer`
    // } else searchUrl = `${BASE_URL}/api/dealers`

    return (
      axios({
        url: searchUrl,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'get'
      })
        .then(response => {
          // setResDatas(response.data);
          console.log('Dealer Data print----- : ', response.data)

          response.data.map(item =>
            // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

            bufferDealer.push([
              item.dealer_id,
              item.name,
              item.mobile_no,
              item.shop_name,
              item.upazila,
              item.contract_status
            ])
          )

          // setDealerList(bufferDealer)
          setDealerList(response.data)

          console.log('---Dealer buffer--- :', dealerList)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('dealer token print----- : ', err)
        })
    )
  }

  const handleClearFilters = () => {
    setSearchKey('')
    setFromFilter('')
    setToFilter('')
    setCheckedRecentAlloted(false)
    setCheckedContractExpiry(false)
    setCheckedFamilyMember(false)
    setExcludeDate('')
    setExcludeDateDealerList([])
    setClearSelected([])

    assignDealersNoFilter()

    setPersonName([])
  }

  const storeAllotment = () => {
    const submitData = {
      allotment_date: values.allotmentDate,
      // distance_from_zonal: distanceZonal,
      fiscal_year: '2021-2022',
      zone: zone,
      district: values.district,
      upazila: values.upazila,
      // selectedDealerIds: selectedDealerIds
      selectedDealerIds: mainSelectedDealerIds,
      productList: inputList,
      total_price: totalPrice,
      ward: ward
    }

    return (
      axios({
        url: `${BASE_URL}/api/allotment/create`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: submitData,
        method: 'post'
      })
        .then(response => {
          console.log('response print----- : ', response.data)
          return Router.push(
            `/admin/allotments?searchAllotment=${query.searchAllotment}&page=${query.page}`
          )
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  let serial = 0

  const handleExcludeDate = e => {
    let bufferDontInclude = []
    // let bufferContractExpire = []
    let today = new Date()
    console.log('e.target.value---: ', e.target.value)
    axios({
      url: `${BASE_URL}/api/allotments/exclude-date-dealers?excludeDate=${e.target.value}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('excludeDate Dealer Data print----- : ', response.data)

        response.data.map(item => {
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          item.allotment_dealers.map(dealer => {
            // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

            bufferDontInclude.push(dealer.dealer_id)

            // const Difference_In_Days =
            //   (new Date(item.product.contract_expiry_date).getTime() -
            //     today.getTime()) /
            //   (1000 * 3600 * 24)

            // console.log('Difference_In_Days----:', Difference_In_Days)

            // if (Difference_In_Days < 30) {
            //   bufferContractExpire.push(item.product_id)
            // }
          })

          bufferDontInclude.push(item.dealer_id)

          // const Difference_In_Days =
          //   (new Date(item.product.contract_expiry_date).getTime() -
          //     today.getTime()) /
          //   (1000 * 3600 * 24)

          // console.log('Difference_In_Days----:', Difference_In_Days)

          // if (Difference_In_Days < 30) {
          //   bufferContractExpire.push(item.product_id)
          // }
        })

        setExcludeDateDealerList(bufferDontInclude)

        // setContractExpiryList(bufferContractExpire)
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        setExcludeDateDealerList([])
        console.log('Recent Alloted print----- : ', err)
      })
  }

  const dontIncludeFilterOne = filterList => {
    return (
      <>
        {dealerList.map(iteam =>
          mainSelectedDealerIds.includes(
            iteam.dealer_id
          ) ? null : filterList.includes(iteam.dealer_id) ||
            recentAllotedDealerList.includes(iteam.dealer_id) ? null : (
            <tr>
              <td>
                <Checkbox
                  checked={
                    selectedDealerIds.includes(iteam.dealer_id) ? true : false
                  }
                  onChange={handleChange}
                  name={iteam.dealer_id}
                  inputProps={{
                    'aria-label': 'primary checkbox'
                  }}
                  // style={{ color: '#4CAF50' }}
                />
              </td>
              <td>{++serial}</td>
              <td>{iteam.dealer_id}</td>
              <td>{iteam.name}</td>
              <td>{iteam.shop_name}</td>
              <td>
                <div style={{ display: 'flex', justifyContent: 'left' }}>
                  {!iteam.show_cause &&
                  !iteam.forgery_proved &&
                  !iteam.flagged ? (
                    <Chip
                      style={{ color: '#4CAF50' }}
                      icon={
                        <FiberManualRecordIcon style={{ color: '#4CAF50' }} />
                      }
                      label='CLEAN'
                    />
                  ) : (
                    ''
                  )}
                  {iteam.show_cause ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#EA80FC',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right',
                        marginRight: '20px'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}

                  {iteam.forgery_proved ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#E65101',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right',
                        marginRight: '20px'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}

                  {iteam.flagged ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#DE3933',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}
                </div>
              </td>
              <td>
                <Chip
                  label={
                    iteam.allotments[0]?.allotment?.allotment_date
                      ? iteam.allotments[0]?.allotment?.allotment_date
                      : 'null'
                  }
                />
              </td>
            </tr>
          )
        )}
      </>
    )
  }
  const dontIncludeFilterTwo = (filterListFirst, filterListSecond) => {
    return (
      <>
        {dealerList.map(iteam =>
          mainSelectedDealerIds.includes(
            iteam.dealer_id
          ) ? null : filterListFirst.includes(iteam.dealer_id) ||
            filterListSecond.includes(iteam.dealer_id) ||
            recentAllotedDealerList.includes(iteam.dealer_id) ? null : (
            <tr>
              <td>
                <Checkbox
                  checked={
                    selectedDealerIds.includes(iteam.dealer_id) ? true : false
                  }
                  onChange={handleChange}
                  name={iteam.dealer_id}
                  inputProps={{
                    'aria-label': 'primary checkbox'
                  }}
                  // style={{ color: '#4CAF50' }}
                />
              </td>
              <td>{++serial}</td>
              <td>{iteam.dealer_id}</td>
              <td>{iteam.name}</td>
              <td>{iteam.shop_name}</td>
              <td>
                <div style={{ display: 'flex', justifyContent: 'left' }}>
                  {!iteam.show_cause &&
                  !iteam.forgery_proved &&
                  !iteam.flagged ? (
                    <Chip
                      style={{ color: '#4CAF50' }}
                      icon={
                        <FiberManualRecordIcon style={{ color: '#4CAF50' }} />
                      }
                      label='CLEAN'
                    />
                  ) : (
                    ''
                  )}
                  {iteam.show_cause ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#EA80FC',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right',
                        marginRight: '20px'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}

                  {iteam.forgery_proved ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#E65101',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right',
                        marginRight: '20px'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}

                  {iteam.flagged ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#DE3933',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}
                </div>
              </td>
              <td>
                <Chip
                  label={
                    iteam.allotments[0]?.allotment?.allotment_date
                      ? iteam.allotments[0]?.allotment?.allotment_date
                      : 'null'
                  }
                />
              </td>
            </tr>
          )
        )}
      </>
    )
  }
  const dontIncludeFilterThree = (
    filterListFirst,
    filterListSecond,
    filterListThird
  ) => {
    return (
      <>
        {dealerList.map(iteam =>
          mainSelectedDealerIds.includes(
            iteam.dealer_id
          ) ? null : filterListFirst.includes(iteam.dealer_id) ||
            filterListSecond.includes(iteam.dealer_id) ||
            filterListThird.includes(iteam.dealer_id) ||
            recentAllotedDealerList.includes(iteam.dealer_id) ? null : (
            <tr>
              <td>
                <Checkbox
                  checked={
                    selectedDealerIds.includes(iteam.dealer_id) ? true : false
                  }
                  onChange={handleChange}
                  name={iteam.dealer_id}
                  inputProps={{
                    'aria-label': 'primary checkbox'
                  }}
                  // style={{ color: '#4CAF50' }}
                />
              </td>
              <td>{++serial}</td>
              <td>{iteam.dealer_id}</td>
              <td>{iteam.name}</td>
              <td>{iteam.shop_name}</td>
              <td>
                <div style={{ display: 'flex', justifyContent: 'left' }}>
                  {!iteam.show_cause &&
                  !iteam.forgery_proved &&
                  !iteam.flagged ? (
                    <Chip
                      style={{ color: '#4CAF50' }}
                      icon={
                        <FiberManualRecordIcon style={{ color: '#4CAF50' }} />
                      }
                      label='CLEAN'
                    />
                  ) : (
                    ''
                  )}
                  {iteam.show_cause ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#EA80FC',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right',
                        marginRight: '20px'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}

                  {iteam.forgery_proved ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#E65101',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right',
                        marginRight: '20px'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}

                  {iteam.flagged ? (
                    <div
                      style={{
                        width: '20px',
                        height: '20px',
                        background: '#DE3933',
                        marginBottom: '20px',
                        borderRadius: '2px',
                        float: 'right'
                      }}
                      className='show_cause_box'
                    ></div>
                  ) : null}
                </div>
              </td>
              <td>
                <Chip
                  label={
                    iteam.allotments[0]?.allotment?.allotment_date
                      ? iteam.allotments[0]?.allotment?.allotment_date
                      : 'null'
                  }
                />
              </td>
            </tr>
          )
        )}
      </>
    )
  }

  const assignDealerList = () => {
    if (
      checkedContractExpiry &&
      !checkedRecentAlloted &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterOne(contractExpiryList)
    } else if (
      checkedFamilyMember &&
      !checkedRecentAlloted &&
      !checkedContractExpiry
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer.family_dealer_id) {
          familyDealerTemp.push(dealer.family_dealer_id)
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer.family_dealer_id)) {
          familyDealerList.push(dealer.dealer_id, dealer.family_dealer_id)
        }
      })
      console.log('familyDealerList----:', familyDealerList)

      return dontIncludeFilterOne(familyDealerList)
    } else if (
      checkedRecentAlloted &&
      !checkedContractExpiry &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterOne(recentAllotedDealerList)
    } else if (
      excludeDateDealerList.length > 0 &&
      !checkedRecentAlloted &&
      !checkedContractExpiry &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterOne(excludeDateDealerList)
    } else if (
      checkedRecentAlloted &&
      checkedContractExpiry &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterTwo(recentAllotedDealerList, contractExpiryList)
    } else if (
      !checkedRecentAlloted &&
      checkedContractExpiry &&
      checkedFamilyMember
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer.family_dealer_id) {
          familyDealerTemp.push(dealer.family_dealer_id)
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer.family_dealer_id)) {
          familyDealerList.push(dealer.dealer_id, dealer.family_dealer_id)
        }
      })
      return dontIncludeFilterTwo(contractExpiryList, familyDealerList)
    } else if (
      checkedRecentAlloted &&
      !checkedContractExpiry &&
      checkedFamilyMember
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer.family_dealer_id) {
          familyDealerTemp.push(dealer.family_dealer_id)
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer.family_dealer_id)) {
          familyDealerList.push(dealer.dealer_id, dealer.family_dealer_id)
        }
      })
      return dontIncludeFilterTwo(recentAllotedDealerList, familyDealerList)
    } else if (
      checkedRecentAlloted &&
      checkedContractExpiry &&
      checkedFamilyMember
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer.family_dealer_id) {
          familyDealerTemp.push(dealer.family_dealer_id)
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer.family_dealer_id)) {
          familyDealerList.push(dealer.dealer_id, dealer.family_dealer_id)
        }
      })
      return dontIncludeFilterThree(
        recentAllotedDealerList,
        contractExpiryList,
        familyDealerList
      )
    } else {
      return (
        <>
          {dealerList.map(iteam =>
            mainSelectedDealerIds.includes(
              iteam.dealer_id
            ) ? null : recentAllotedDealerList.includes(
                iteam.dealer_id
              ) ? null : (
              <tr>
                <td>
                  <Checkbox
                    checked={
                      selectedDealerIds.includes(iteam.dealer_id) ? true : false
                    }
                    onChange={handleChange}
                    name={iteam.dealer_id}
                    inputProps={{
                      'aria-label': 'primary checkbox'
                    }}
                    // style={{ color: '#4CAF50' }}
                  />
                </td>
                <td>{++serial}</td>
                <td>{iteam.dealer_id}</td>
                <td>{iteam.name}</td>
                <td>{iteam.shop_name}</td>
                <td>
                  <div style={{ display: 'flex', justifyContent: 'left' }}>
                    {!iteam.show_cause &&
                    !iteam.forgery_proved &&
                    !iteam.flagged ? (
                      <Chip
                        style={{ color: '#4CAF50' }}
                        icon={
                          <FiberManualRecordIcon style={{ color: '#4CAF50' }} />
                        }
                        label='CLEAN'
                      />
                    ) : (
                      ''
                    )}
                    {iteam.show_cause ? (
                      <div
                        style={{
                          width: '20px',
                          height: '20px',
                          background: '#EA80FC',
                          marginBottom: '20px',
                          borderRadius: '2px',
                          float: 'right',
                          marginRight: '20px'
                        }}
                        className='show_cause_box'
                      ></div>
                    ) : null}

                    {iteam.forgery_proved ? (
                      <div
                        style={{
                          width: '20px',
                          height: '20px',
                          background: '#E65101',
                          marginBottom: '20px',
                          borderRadius: '2px',
                          float: 'right',
                          marginRight: '20px'
                        }}
                        className='show_cause_box'
                      ></div>
                    ) : null}

                    {iteam.flagged ? (
                      <div
                        style={{
                          width: '20px',
                          height: '20px',
                          background: '#DE3933',
                          marginBottom: '20px',
                          borderRadius: '2px',
                          float: 'right'
                        }}
                        className='show_cause_box'
                      ></div>
                    ) : null}
                  </div>
                </td>
                <td>
                  <Chip
                    label={
                      iteam.allotments[0]?.allotment?.allotment_date
                        ? iteam.allotments[0]?.allotment?.allotment_date
                        : 'null'
                    }
                  />
                </td>
              </tr>
            )
          )}
        </>
      )
    }
  }

  return (
    <GridContainer>
      <div>
        {/* <button type='button' onClick={handleAdd}>
          react-transition-group
        </button> */}

        {/* -- Assign dealer popup -- */}
        <Modal
          aria-labelledby='transition-modal-title'
          aria-describedby='transition-modal-description'
          className={classesModal.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={open}>
            <div className={classesModal.paper}>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <Card>
                    <CardHeader
                      plain
                      color='primary'
                      style={{
                        background: '#898b8a',
                        boxShadow:
                          '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
                      }}
                    >
                      <h4 className={classes.cardTitleWhite}>Assign Dealers</h4>
                      {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
                    </CardHeader>
                    <CardBody>
                      <br />
                      {/* <FormControl className={classes.formControl}>
                        <InputLabel id='demo-mutiple-checkbox-label'>
                          Tag
                        </InputLabel>
                        <Select
                          labelId='demo-mutiple-checkbox-label'
                          id='demo-mutiple-checkbox'
                          multiple
                          value={personName}
                          // onChange={event => setPersonName(event.target.value)}
                          onChange={event => handleDontInclude(event)}
                          input={<Input />}
                          // renderValue={selected => selected.join(', ')}
                          renderValue={selected => selected.length}
                          MenuProps={MenuProps}
                        >
                          {names.map(name => (
                            <MenuItem key={name} value={name}>
                              <Checkbox
                                checked={personName.indexOf(name) > -1}
                              />
                              <ListItemText primary={name} />
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl> */}
                      {/* <br />
                      <br />
                      <br /> */}
                      <br />
                      <div
                        // style={{
                        //   display: 'flex',
                        //   marginTop: '20px',
                        //   justifyContent: 'space-between'
                        // }}
                        className='dealer_filter_wrap'
                      >
                        <div
                          className={classes.search}
                          style={{ height: '56px' }}
                        >
                          <div className={classes.searchIcon}>
                            <SearchIcon />
                          </div>
                          <InputBase
                            placeholder='Enter dealer id/name'
                            classes={{
                              root: classes.inputRoot,
                              input: classes.inputInput
                            }}
                            value={searchKey}
                            inputProps={{ 'aria-label': 'search' }}
                            name='search_key'
                            onChange={e => {
                              setSearchKey(e.target.value)
                              e.target.value.length > 2
                                ? handleSearch(e)
                                : assignDealersNoFilter()
                            }}
                            style={{ marginTop: '2px' }}
                          />
                        </div>
                        <TextField
                          style={{ maxWidth: '80px' }}
                          id='standard-basic'
                          label='From'
                          name='searchFrom'
                          value={fromFilter}
                          variant='outlined'
                          onChange={e => setFromFilter(e.target.value)}
                          InputLabelProps={{
                            shrink: true
                          }}
                          className='max_width_100 margin_b_20 margin_t_20'
                        />
                        <TextField
                          style={{ maxWidth: '80px' }}
                          id='standard-basic'
                          label='To'
                          name='searchTo'
                          value={toFilter}
                          variant='outlined'
                          onChange={e => {
                            setToFilter(e.target.value)
                            handleSearch(e)
                            // setTimeout(() => {
                            //   handleSearch(e)
                            // }, 2000)
                          }}
                          InputLabelProps={{
                            shrink: true
                          }}
                          className='max_width_100 margin_b_20'
                        />

                        <div style={{ marginTop: '-3px' }} className='max_width_100 margin_b_10'>
                          <FormControl
                            fullWidth
                            variant='outlined'
                            className={classes.formControl}
                          >
                            <InputLabel id='demo-mutiple-checkbox-label'>
                              Don't include
                            </InputLabel>
                            <Select
                              label="Don't include"
                              labelId='demo-mutiple-checkbox-label'
                              id='demo-mutiple-checkbox'
                              multiple
                              value={personName}
                              onChange={event => {
                                // setPersonName(event.target.value)
                                handleDontInclude(event)
                              }}
                              // input={<Input />}
                              inputProps={{
                                // name: 'dontInclude',
                                id: 'outlined-age-native-simple'
                              }}
                              // ------------------------------------

                              // {...(checkedRecentAlloted ||
                              // checkedContractExpiry ||
                              // checkedFamilyMember
                              //   ? {
                              //       renderValue: selected =>
                              //         `Selected : ${selected.length}`
                              //     }
                              //   : { renderValue: () => '' })}

                              // -------------------------------------------------

                              renderValue={selected =>
                                selected && `Selected : ${selected.length}`
                              }
                              MenuProps={MenuProps}
                              style={{ width: '140px', marginTop: '4px' }}
                              className='max_width_100 margin_b_10'
                            >
                              <MenuItem value={'recentAllotedDealers'}>
                                <Checkbox
                                  // checked={state.item.dealer_id}
                                  checked={checkedRecentAlloted}
                                  // onChange={handleDontInclude}
                                  name='recentAllotedDealers'
                                  inputProps={{
                                    'aria-label': 'primary checkbox'
                                  }}
                                  style={{ color: '#4CAF50' }}
                                />
                                Recently allotted dealers
                              </MenuItem>
                              <MenuItem value={'contractExpiry'}>
                                <Checkbox
                                  // checked={state.item.dealer_id}
                                  checked={checkedContractExpiry}
                                  // onChange={handleDontInclude}
                                  name='contractExpiry'
                                  inputProps={{
                                    'aria-label': 'primary checkbox'
                                  }}
                                  style={{ color: '#4CAF50' }}
                                />
                                Contract expires in 1 month
                              </MenuItem>
                              <MenuItem value={'sameFamilyMember'}>
                                <Checkbox
                                  // checked={state.item.dealer_id}
                                  checked={checkedFamilyMember}
                                  // onChange={handleDontInclude}
                                  name='sameFamilyMember'
                                  inputProps={{
                                    'aria-label': 'primary checkbox'
                                  }}
                                  style={{ color: '#4CAF50' }}
                                />
                                Same family members
                              </MenuItem>
                            </Select>
                          </FormControl>
                        </div>

                        {/* <InputLabel
                        style={{ marginBottom: '10px' }}
                        htmlFor='outlined-age-simple'
                      >
                        Don't include
                      </InputLabel>
                      <Select
                        // value={values.distanceZonal}
                        // onChange={e => setDistanceZonal(e.target.value)}
                        onChange={handleInputValue}
                        onBlur={handleInputValue}
                        // name='distanceZonal'
                        multiple
                        value={personName}
                        input={
                          <OutlinedInput
                            name='dontInclude'
                            id='outlined-age-simple'
                          />
                        }
                      >
                        <MenuItem value={'recentAllotedDealers'}>
                          <Checkbox
                            // checked={state.item.dealer_id}
                            checked={checkedRecentAlloted}
                            onChange={handleDontInclude}
                            name='recentAllotedDealers'
                            inputProps={{
                              'aria-label': 'primary checkbox'
                            }}
                            style={{ color: '#4CAF50' }}
                          />
                          Recently allotted dealers
                        </MenuItem>
                        <MenuItem value={'contractExpiry'}>
                          <Checkbox
                            // checked={state.item.dealer_id}
                            checked={checkedContractExpiry}
                            onChange={handleDontInclude}
                            name='contractExpiry'
                            inputProps={{
                              'aria-label': 'primary checkbox'
                            }}
                            style={{ color: '#4CAF50' }}
                          />
                          Contract expires in 1 month
                        </MenuItem>
                        <MenuItem value={'sameFamilyMember'}>
                          <Checkbox
                            // checked={state.item.dealer_id}
                            checked={checkedFamilyMember}
                            onChange={handleDontInclude}
                            name='sameFamilyMember'
                            inputProps={{
                              'aria-label': 'primary checkbox'
                            }}
                            style={{ color: '#4CAF50' }}
                          />
                          Same family members
                        </MenuItem>
                      </Select> */}

                        <TextField
                          id='date'
                          label='Exclude date'
                          type='date'
                          variant='outlined'
                          // defaultValue='2021-12-09'
                          value={excludeDate}
                          className={`${classes.textField} max_width_100 margin_b_10 margin_l_0`}
                          InputLabelProps={{
                            shrink: true
                          }}
                          onChange={e => {
                            setExcludeDate(e.target.value)
                            handleExcludeDate(e)
                          }}
                          // onChange={handleExcludeDate}
                          // onBlur={handleInputValue}
                          name='excludeDate'
                          style={{ marginLeft: '50px' }}
                          // className='max_width_100 margin_b_10 margin_l_0'
                        />
                        <Button
                          color='primary'
                          style={{
                            background: 'transparent',
                            // color: '#4CAF50',
                            color: '#000',
                            height: 'fit-content',
                            padding: '10px 16px',
                            border: '1px solid #D3D3D3',
                            textTransform: 'none',
                            borderRadius: '20px',
                            boxShadow: 'none'
                          }}
                          // onClick={() => {
                          //   handleAssignDealers()
                          //   handleClose()
                          // }}
                          // onClick={e => handleClearFilters(e)}
                          onClick={handleClearFilters}
                        >
                          Clear filters
                        </Button>
                      </div>
                      <br />
                      <br />
                      <div className={'assign_dealers'}>
                        <table>
                          <tr>
                            <th>Select</th>
                            <th>Serial</th>
                            <th>Dealer Id</th>
                            <th>Dealer Name</th>
                            <th>Shop name</th>
                            <th>Dealer status</th>
                            <th>Recent allotment</th>
                          </tr>
                          {assignDealerList()}
                        </table>
                      </div>
                      <br />
                      <br />
                      <Pagination
                        count={lastPage}
                        page={page}
                        onChange={assignDealersNoFilter}
                      />
                    </CardBody>
                    <CardFooter>
                      {/* <Link href='/admin/add-new-dealer'> */}
                      <Button
                        color='primary'
                        style={{ background: '#4CAF50' }}
                        onClick={() => {
                          handleAssignDealers()
                          handleClose()
                        }}
                      >
                        Assign
                      </Button>
                      <Button
                        color='secondary'
                        // style={{ background: '#4CAF50' }}
                        onClick={() => handleClose()}
                      >
                        Cancel
                      </Button>
                      {/* </Link> */}
                    </CardFooter>
                  </Card>
                </GridItem>
              </GridContainer>
            </div>
          </Fade>
        </Modal>
        {/* -- save Allotment popup -- */}
        <Modal
          aria-labelledby='transition-modal-title'
          aria-describedby='transition-modal-description'
          className={classesModal.modal}
          open={openSaveModal}
          onClose={handleCloseSaveModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={openSaveModal}>
            <div className={classesModal.paperSave}>
              <h2 id='transition-modal-title'>Confirm?</h2>
              <p id='transition-modal-description'>
                Save all the information & add the dealer.
                {console.log('setSelectedDealersNo----', selectedDealersNo)}
              </p>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  startIcon={<CancelIcon />}
                  onClick={() => handleCloseSaveModal()}
                >
                  Cancel
                </Button>
                <Button
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  style={{ background: '#00AC34', marginLeft: '20px' }}
                  startIcon={<SaveIcon />}
                  onClick={() => storeAllotment()}
                >
                  Save
                </Button>
              </div>
              <br />
              <Button
                fullWidth
                variant='contained'
                color='secondary'
                // className={classes.button}
                style={{
                  background: 'transparent',
                  textTransform: 'none',
                  color: '#00AC34',
                  border: '1px solid #00AC34'
                }}
                startIcon={<SendIcon />}
                onClick={() => {
                  sendBulkSms()
                  storeAllotment()
                }}
              >
                Send Confirmation SMS & save
              </Button>
            </div>
          </Fade>
        </Modal>

        {/* -- Remove Product popup -- */}
        <Modal
          aria-labelledby='transition-modal-title'
          aria-describedby='transition-modal-description'
          className={classesModal.modal}
          open={openProductRemoveModal}
          onClose={handleCloseSaveModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={openProductRemoveModal}>
            <div className={classesModal.paperSave}>
              <h2 id='transition-modal-title'>Confirm?</h2>
              <p id='transition-modal-description'>
                Remove the product with all the information.
              </p>
              <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                startIcon={<CancelIcon />}
                onClick={() => handleCloseProductRemoveModal()}
              >
                Cancel
              </Button>
              <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                style={{ background: '#ff3a3a', marginLeft: '20px' }}
                startIcon={<SaveIcon />}
                onClick={() => handleRemoveClick(deleteIndex)}
              >
                Remove
              </Button>
            </div>
          </Fade>
        </Modal>
      </div>

      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color='primary' style={{ background: '#4CAF50' }}>
            <h4 className={classes.cardTitleWhite}>Add new allotment</h4>
            <p className={classes.cardCategoryWhite}>
              {/* Here is a subtitle for this table */}
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor='primary'
              tableHead={[
                'Product name',
                'Allotment amount kg/litre',
                'Price per kg/litre (in taka)',
                'Transport cost (in taka)',
                'Total price (in taka)',
                'Actions'
              ]}
              // tableData={[
              //   ["Rice", "200 kg", "35", "5.00", "7005"],
              //   ["Potato", "200 kg", "35", "5.00", "7005"],

              //   [<Select fullWidth
              //     // value={{
              //     //   age: '',
              //     //   name: 'hai',
              //     //   labelWidth: 0,
              //     // }}
              //     value={'new_dealer'}
              //     // onChange={this.handleChange}
              //     input={
              //       <OutlinedInput
              //         // labelWidth={100}
              //         name="age"
              //         id="outlined-age-simple"
              //       />
              //     }
              //   >

              //     <MenuItem value={'permant'}>Sugar</MenuItem>
              //     <MenuItem value={'new_dealer'}>Rice</MenuItem>
              //   </Select>,
              //   <div style={{ display: 'flex'}}>
              //   <TextField id="standard-basic" label="" variant="outlined" onChange={e => setShopName(e.target.value)} />

              //   <Select fullWidth
              //   // value={{
              //   //   age: '',
              //   //   name: 'hai',
              //   //   labelWidth: 0,
              //   // }}
              //   value={'permant'}
              //   // onChange={this.handleChange}
              //   input={
              //     <OutlinedInput
              //       // labelWidth={100}
              //       name="age"
              //       id="outlined-age-simple"
              //     />
              //   }
              // >

              //   <MenuItem value={'permant'}>Kg</MenuItem>
              //   <MenuItem value={'new_dealer'}>Litre</MenuItem>
              // </Select>
              // </div>,
              // <TextField id="standard-basic" label="" variant="outlined" onChange={e => setShopName(e.target.value)} />,
              // <TextField id="standard-basic" label="" variant="outlined" onChange={e => setShopName(e.target.value)} />
              // , ""],

              // ]}
              tableData={resDatas}
            />

            <GridContainer>
              <GridItem xs={9} sm={9} md={9}>
              <div className='subtotal_left'>
                <p style={{ textAlign: 'right', marginRight: '0px' }}>
                  Subtotal :{' '}
                </p>
                <p style={{ textAlign: 'right', marginRight: '0px' }}>
                  According to ordinance, 1984 subsection 53(E) items total
                  price x 5% x 5% deducted from source :{' '}
                </p>
                <p style={{ textAlign: 'right', marginRight: '0px' }}>
                  Grand Total :{' '}
                </p>
                </div>
              </GridItem>
              <GridItem xs={3} sm={3} md={3} style={{ paddingLeft: '5px !important', paddingRight: '0px !important' }}>
                <div className='subtotal_right'>
                  <p
                    style={{ fontWeight: 'bold' }}
                    className='subtotal_txt'
                  >
                    {subTotal}
                  </p>
                  <p
                    style={{ fontWeight: 'bold'}}
                    className='subtotal_txt'
                  >
                    {subTotal * 0.0025}
                  </p>
                  <p
                    style={{ fontWeight: 'bold'}}
                    className='subtotal_txt'
                  >
                    {subTotal + subTotal * 0.0025}
                  </p>
                </div>
              </GridItem>
            </GridContainer>

            {/* <GridItem xs={12} sm={12} md={12}> */}
            {/* <div style={{ display: 'flex', justifyContent: 'right' }}> */}
            {/* <p style={{ textAlign: 'right', marginRight: '140px' }}>
              Subtotal :{' '}
              <span style={{ fontWeight: 'bold', marginLeft: '60px' }}>
                {subTotal}
              </span>
            </p>
            <p style={{ textAlign: 'right', marginRight: '140px' }}>
              According to ordinance, 1984 subsection 53(E) items total price x
              5% x 5% deducted from source :{' '}
              <span style={{ fontWeight: 'bold', marginLeft: '60px' }}>
                {subTotal * 0.0025}
              </span>
            </p>
            <p style={{ textAlign: 'right', marginRight: '140px' }}>
              Grand Total :{' '}
              <span style={{ fontWeight: 'bold', marginLeft: '60px' }}>
                {subTotal + subTotal * 0.0025}
              </span>
            </p> */}

            {/* </div> */}
            {/* </GridItem> */}
          </CardBody>
          <CardFooter>
            {/* <Link href='/admin/add-new-dealer'> */}
            <Button
              color='primary'
              style={{ background: '#4CAF50' }}
              onClick={handleAddClick}
            >
              Add Product
            </Button>
            {/* </Link> */}
          </CardFooter>
        </Card>
      </GridItem>
      {/* <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="primary" style={{ background: '#4CAF50'}}>
            <h4 className={classes.cardTitleWhite}>
              Allotment Information
            </h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["ID", "Name", "Country", "City", "Salary"]}
              tableData={[
                ["1", "Dakota Rice", "$36,738", "Niger", "Oud-Turnhout"],
                ["2", "Minerva Hooper", "$23,789", "Curaçao", "Sinaai-Waas"],
                ["3", "Sage Rodriguez", "$56,142", "Netherlands", "Baileux"],
                [
                  "4",
                  "Philip Chaney",
                  "$38,735",
                  "Korea, South",
                  "Overland Park",
                ],
                [
                  "5",
                  "Doris Greene",
                  "$63,542",
                  "Malawi",
                  "Feldkirchen in Kärnten",
                ],
                ["6", "Mason Porter", "$78,615", "Chile", "Gloucester"],
              ]}
            />
          </CardBody>
        </Card>
      </GridItem> */}

      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader
            plain
            color='primary'
            style={{
              background: '#898b8a',
              boxShadow:
                '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
            }}
          >
            <h4 className={classes.cardTitleWhite}>Allotment Information</h4>
            {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
          </CardHeader>
          <CardBody>
            <br />
            <br />
            <GridContainer></GridContainer>
            <GridContainer>
              <GridItem xs={12} sm={12} md={3}>
                <TextField
                  id='date'
                  label='Allotment date'
                  fullWidth
                  type='date'
                  variant='outlined'
                  // defaultValue='2021-12-09'
                  // onChange={e => setAllotmentDate(e.target.value)}
                  onChange={handleInputValue}
                  onBlur={handleInputValue}
                  name='allotmentDate'
                  error={errors['allotmentDate']}
                  {...(errors['allotmentDate'] && {
                    error: true,
                    helperText: errors['allotmentDate']
                  })}
                  className={`${classes.textField} margin-btm-sm`}
                  InputLabelProps={{
                    shrink: true
                  }}
                  inputProps={{
                    min: new Date().toISOString().slice(0, 10)
                  }}
                />
              </GridItem>

              <GridItem xs={12} sm={12} md={3}>
                <TextField
                  InputLabelProps={{ shrink: true }}
                  id='standard-basic'
                  label='Zone'
                  variant='outlined'
                  disabled
                  value={zone}
                  fullWidth
                  onChange={e => setZone(e.target.value)}
                />{' '}
                <br /> <br />
              </GridItem>

              <GridItem xs={12} sm={12} md={3}>
                <FormControl
                  fullWidth
                  variant='outlined'
                  className={`${classes.formControl} margin-btm-sm`}
                >
                  <InputLabel
                    style={{ marginBottom: '10px' }}
                    htmlFor='outlined-age-simple'
                  >
                    District
                  </InputLabel>
                  <Select
                    label='District'
                    fullWidth
                    value={values.district}
                    // onChange={e => setDistrict(e.target.value)}
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='district'
                    error={errors['district']}
                    {...(errors['district'] && {
                      error: true,
                      helperText: errors['district']
                    })}
                    inputProps={{
                      name: 'district',
                      id: 'outlined-age-native-simple'
                    }}
                  >
                    {districtList.map(item => (
                      <MenuItem value={item.name} key={item.id}>
                        {item.name}
                      </MenuItem>
                    ))}

                    {/* <MenuItem value={'dhaka'}>Dhaka</MenuItem>
                    <MenuItem value={'rajshahi'}>Rajshahi</MenuItem>
                    <MenuItem value={'dinajpur'}>Dinajpur</MenuItem>
                    <MenuItem value={'mymensingh'}>Mymensingh</MenuItem>
                    <MenuItem value={'naogaon'}>Naogaon</MenuItem>
                    <MenuItem value={'faridpur'}>Faridpur</MenuItem> */}
                  </Select>
                </FormControl>
              </GridItem>
              <GridItem xs={12} sm={12} md={3}>
                <FormControl
                  fullWidth
                  variant='outlined'
                  className={`${classes.formControl} margin-btm-sm`}
                >
                  <InputLabel
                    style={{ marginBottom: '10px' }}
                    htmlFor='outlined-age-simple'
                  >
                    Upazila
                  </InputLabel>
                  <Select
                    label='Upazila'
                    fullWidth
                    value={values.upazila}
                    // onChange={e => setUpazila(e.target.value)}
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='upazila'
                    error={errors['upazila']}
                    {...(errors['upazila'] && {
                      error: true,
                      helperText: errors['upazila']
                    })}
                    inputProps={{
                      name: 'upazila',
                      id: 'outlined-age-native-simple'
                    }}
                  >
                    <MenuItem value={''}>Select</MenuItem>
                    {upazilaList.map(item =>
                      item.district == values.district
                        ? item.upazilas.map(itemUpz => (
                            <MenuItem value={itemUpz.name} key={itemUpz.id}>
                              {itemUpz.name}
                            </MenuItem>
                          ))
                        : null
                    )}

                    {/* <MenuItem value={'adamdighi'}>Adamdighi</MenuItem>
                    <MenuItem value={'habiganj'}>Habiganj</MenuItem>
                    <MenuItem value={'bagha'}>Bagha</MenuItem>
                    <MenuItem value={'mymensingh'}>Mymensingh</MenuItem>
                    <MenuItem value={'mohadevpur'}>Mohadevpur</MenuItem>
                    <MenuItem value={'tanore'}>Tanore</MenuItem>
                    <MenuItem value={'badalgachi'}>Badalgachi</MenuItem> */}
                  </Select>
                </FormControl>
              </GridItem>
              <GridItem xs={12} sm={12} md={3}>
                <TextField
                  InputLabelProps={{ shrink: true }}
                  id='standard-basic'
                  label='Allotment Id'
                  variant='outlined'
                  disabled
                  value={allotmentId + 1}
                  fullWidth
                  // onChange={e => setZone(e.target.value)}
                />
                <br /> <br />
              </GridItem>
              <GridItem xs={12} sm={12} md={3}>
              <TextField
                    id='standard-basic'
                    type='number'
                    label='Ward'
                    variant='outlined'
                    fullWidth
                    onChange={e => setWard(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    name='ward'
                    // error={errors['shopName']}
                    // {...(errors['shopName'] && {
                    //   error: true,
                    //   helperText: errors['shopName']
                    // })}
                  />
                <br /> <br />
              </GridItem>
              {/* <GridItem xs={12} sm={12} md={3}>
                <InputLabel
                  style={{ marginBottom: '10px' }}
                  htmlFor='outlined-age-simple'
                >
                  Distance From zonal office
                </InputLabel>
                <Select
                    fullWidth
                    value={values.distanceZonal}
                    // onChange={e => setDistanceZonal(e.target.value)}
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    // name='distanceZonal'
                    error={errors['distanceZonal']}
                    {...(errors['distanceZonal'] && {
                      error: true,
                      helperText: errors['distanceZonal']
                    })}
                    input={
                      <OutlinedInput
                        name='distanceZonal'
                        id='outlined-age-simple'
                      />
                    }
                  >
                  <MenuItem value={'0-50'}>0 - 50 km</MenuItem>
                  <MenuItem value={'51-100'}>51 - 100 km</MenuItem>
                  <MenuItem value={'100+'}>100+ km</MenuItem>
                </Select>
              </GridItem> */}
            </GridContainer>
          </CardBody>
          <CardFooter></CardFooter>
        </Card>
      </GridItem>

      {mainSelectedDealerIds.length > 0 ? (
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader
              plain
              color='primary'
              style={{
                background: '#898b8a',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Assigned Dealers</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <div className={'assign_dealers'}>
                <table>
                  <tr>
                    <th>Select Dealer</th>
                    <th>Serial</th>
                    <th>Dealer Id</th>
                    <th>Dealer Name</th>
                    <th>Shop name</th>
                    <th>Status</th>
                    <th>Payment</th>
                  </tr>
                  {dealerList.map(iteam =>
                    mainSelectedDealerIds.includes(iteam.dealer_id) ? (
                      <tr>
                        <td>
                          {/* <button type='button' onClick={handleAdd}>
                  test
                </button> */}
                          <Checkbox
                            // checked={state.item.dealer_id}
                            // checked={true}
                            checked={
                              removeDealerIds.includes(iteam.dealer_id)
                                ? true
                                : false
                            }
                            // value={item.dealer_id}
                            // onChange={() => {
                            //   const newTodos = [...todos]
                            //   newTodos.push('ashraf')
                            //   setTodos(newTodos)
                            //   console.log('newTodos--', todos)
                            // }}
                            onChange={handleRemoveChange}
                            name={iteam.dealer_id}
                            inputProps={{
                              'aria-label': 'primary checkbox'
                            }}
                            // style={{ color: '#4CAF50' }}
                          />
                        </td>
                        <td>{++serial}</td>
                        <td>{iteam.dealer_id}</td>
                        <td>{iteam.name}</td>
                        <td>{iteam.shop_name}</td>
                      </tr>
                    ) : null
                  )}
                </table>
              </div>
            </CardBody>
            {removeDealerIds.length > 0 ? (
              <CardFooter>
                {/* <Link href='/admin/add-new-dealer'> */}
                <Button
                  color='primary'
                  style={{ background: '#F0383B' }}
                  onClick={() => {
                    handleRemoveDealers()
                  }}
                >
                  Remove
                </Button>
                <Button
                  color='primary'
                  style={{ background: '#4CAF50' }}
                  onClick={() => handleCancelRemove()}
                >
                  Cancel
                </Button>
                {/* </Link> */}
              </CardFooter>
            ) : null}
          </Card>
        </GridItem>
      ) : null}

      <GridItem xs={12} sm={12} md={12}>
        <br />
        <div style={{ textAlign: 'center' }}>
          <Button
            color='primary'
            onClick={() => {
              assignDealersNoFilter()
              handleOpen()
            }}
            style={{ background: '#4CAF50', textAlign: 'center' }}
            disabled={!formIsValid()}
          >
            Assign dealers
          </Button>
        </div>

        <br />
        <br />
      </GridItem>
      <GridItem xs={12} sm={12} md={12} style={{ marginTop: '20px' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Button
            color='primary'
            // onClick={() => storeAllotment()}
            onClick={handleOpenSaveModal}
            style={{ background: '#4CAF50' }}
            // disabled={mainSelectedDealerIds.length == 0 ? true : false}
            // disabled={subTotal == 0 ? true : false}
            // disabled={0 <= 0 ? true : false}
            // disabled={!formIsValid()}

            disabled={
              subTotal == 0 || mainSelectedDealerIds.length == 0 ? true : false
            }
          >
            Save Allotment{' '}
            {console.log('mainSelectedDealerIds-----:', mainSelectedDealerIds)}
          </Button>
          <Link
            href={`/admin/allotments?searchAllotment=${query.searchAllotment}&page=${query.page}`}
          >
            <Button
              color='primary'
              style={{ background: '#898B8A' }}
              // onClick={() => handleClose()}
            >
              Back
            </Button>
          </Link>
        </div>
      </GridItem>
      {console.log('inputList----', inputList)}
    </GridContainer>
  )
}

AddNewAllotment.layout = Admin

export default AddNewAllotment
