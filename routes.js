/*!

=========================================================
* * NextJS Material Dashboard v1.1.0 based on Material Dashboard React v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/nextjs-material-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/nextjs-material-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import PeopleIcon from '@material-ui/icons/People';

//custom google material icon
import ManageAccounts from "assets/svg/manage_accounts_white_24dp.svg";

const dashboardRoutes = [
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   rtlName: "لوحة القيادة",
  //   icon: Dashboard,

  //   layout: "/admin",
  // },
  // {
  //   path: "/user-profile",
  //   name: "User Profile",
  //   rtlName: "ملف تعريفي للمستخدم",
  //   icon: Person,

  //   layout: "/admin",
  // },
  {
    path: "/dealers",
    name: "Dealers",
    rtlName: "قائمة الجدول",
    icon: PeopleIcon,

    layout: "/admin",
    permissions: ['superadmin', 'admin'],
  },
  {
    path: "/allotments",
    name: "Allotments",
    rtlName: "طباعة",
    icon: LocalShippingIcon,

    layout: "/admin",
    permissions: ['superadmin', 'admin'],
  },
  {
    path: "/users",
    name: "Users",
    rtlName: "",
    // icon: BubbleChart,
    icon: 'ManageAccounts',

    layout: "/admin",
    permissions: ['superadmin'],
  },
  {
    path: "/verify",
    name: "Verify",
    rtlName: "قائمة الجدول",
    icon: PeopleIcon,

    layout: "/admin",
    permissions: ['warehouse'],
  },
  {
    path: "/warehouse-allotments",
    name: "Allotments",
    rtlName: "طباعة",
    icon: LocalShippingIcon,

    layout: "/admin",
    permissions: ['warehouse'],
  },
  {
    path: "/account",
    name: "Account",
    rtlName: "",
    icon: PeopleIcon,
    icon: 'AdminPanel',

    layout: "/admin",
    permissions: ['admin', 'warehouse'],
  },

  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,

  //   layout: "/admin",
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,

  //   layout: "/admin",
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,

  //   layout: "/rtl",
  // },
  // {
  //   path: "/upgrade-to-pro",
  //   name: "Upgrade To PRO",
  //   rtlName: "التطور للاحترافية",
  //   icon: Unarchive,

  //   layout: "/admin",
  // },
];

export default dashboardRoutes;
